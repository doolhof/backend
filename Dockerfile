FROM openjdk:11
VOLUME /tmp
ADD /build/libs/backend-0.0.1.jar app.jar
RUN adduser --disabled-login myuser
USER myuser
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
CMD java -Dserver.port=$PORT -jar app.jar