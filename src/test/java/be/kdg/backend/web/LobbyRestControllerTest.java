package be.kdg.backend.web;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.web.dto.GameSettingDto;
import be.kdg.backend.web.dto.LobbyDto;
import be.kdg.backend.web.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class LobbyRestControllerTest {
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    UserService userService;
    @Test
    public void testFindLobbyById() throws Exception {
        Lobby lobby = new Lobby();
        lobby.setLobbyId(1);
        lobbyService.saveLobby(lobby);

        this.mockMvc.perform(get("/api/lobby/"+1))
                .andDo(print())
                .andExpect(jsonPath("lobbyId", is(lobby.getLobbyId())))
                .andExpect(status().isOk());


    }

    @Test
    public void testCreateLobby() throws Exception {
        this.mockMvc.perform(post("/api/lobby/createLobby/"+1+"/"+2+"/"+30))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void testGetAllLobbiesFromUser() throws Exception {
        User user = userService.findUserById(1);
        List<User> users = new ArrayList<>();
        users.add(user);
        List<Lobby> lobbies = new ArrayList<>();
        Lobby lobby = new Lobby();
        lobby.setListUsers(users);
        lobbies.add(lobby);
        user.setLobbies(lobbies);
        this.mockMvc.perform(get("/api/lobby/loadLobbies/" + user.getUsername()))
                .andDo(print())
                .andExpect(status().isOk());

    }
}
