package be.kdg.backend.web;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.apache.bcel.util.Play;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PlayerRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PlayerService playerService;

    @Autowired
    private TileService tileService;


    @Test
    public void moveWithPath() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        int destination = 21;
        if (player.getCurrentPosition() == 21) {
            destination = 14;
        } else {
            destination = 21;
        }
        mockMvc.perform(post("/api/player/move?playerId=" + player.getPlayerId() + "&destination=" + destination))
                .andExpect(status().isOk())
                .andReturn();

    }


    @Test
    @Transactional
    public void moveNoPath() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), 1);
        mockMvc.perform(post("/api/player/move?gameId=" + player.getGame().getGameId() + "&playerId=" + player.getPlayerId() + "&destination=" + destinationTile.getPosition()))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    @Transactional
    public void testPlayerFoundTreasure() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        List<Treasure> treasures = new ArrayList<>();
        treasures.add(new Treasure(1,"schatkist"));
        player.setToFind(treasures);
        playerService.savePlayer(player);
        mockMvc.perform(post("/api/player/treasureSearch?playerId=" + player.getPlayerId()))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @Transactional
    public void testPlayerWonGame() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        List<Treasure> treasures = new ArrayList<>();
        player.setToFind(treasures);
        player.setStartPosition(7);
        player.setCurrentPosition(7);
        List<Player> players = new ArrayList<>();
        players.add(player);
        player.getGame().setPlayerList(players);
        playerService.savePlayer(player);
        MvcResult mvcResult = mockMvc.perform(post("/api/player/checkWinGame?playerId=" + player.getPlayerId()))
                .andExpect(status().isOk())
                .andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Player won",result,"true");
    }

    @Test
    @Transactional
    public void testPlayerNotOnStartPositionButFoundAllTreasures() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        List<Treasure> treasures = new ArrayList<>();
        player.setToFind(treasures);
        player.setStartPosition(7);
        player.setCurrentPosition(8);
        playerService.savePlayer(player);
       MvcResult mvcResult =  mockMvc.perform(post("/api/player/checkWinGame?playerId=" + player.getPlayerId()))
                .andExpect(status().isOk())
                .andReturn();
       String result = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Player didn't win",result,"false");
    }

    @Test
    @Transactional
    public void testPlayerOnStartPositionButNotFoundAllTreasures() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        List<Treasure> treasures = new ArrayList<>();
        treasures.add(new Treasure(3,"schatkist"));
        player.setToFind(treasures);
        player.setStartPosition(14);
        player.setCurrentPosition(14);
        Player playerSaved = playerService.savePlayer(player);
        MvcResult mvcResult =  mockMvc.perform(post("/api/player/checkWinGame?playerId=" + playerSaved.getPlayerId()))
                .andExpect(status().isOk())
                .andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("Player didn't win",result,"false");
    }

    @Test
    @Transactional
    public void testPlayerMovedIfMazecardInserted() throws Exception {
        Player player = playerService.getPlayerByUserId(1);
        player.setCurrentPosition(2);
        Player playerSaved = playerService.savePlayer(player);
        MvcResult mvcResult =  mockMvc.perform(post("/api/player/movePlayerTile?playerId=" + playerSaved.getPlayerId() + "&destination=" + 2))
                .andExpect(status().isOk())
                .andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        result = result.substring(1, result.length()-1);
        JSONObject jsonObject = new JSONObject(result);
        int currentPosition = jsonObject.getInt("currentPosition");
        Assert.assertTrue(currentPosition==9);
    }
}