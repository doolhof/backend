
package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.restControllers.LobbyRestController;
import be.kdg.backend.web.socketMessages.ChatMessage;
import be.kdg.backend.web.socketMessages.PartyClientMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import javax.annotation.Nonnull;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ChatWebSocketControllerTest {


    @Value("${local.server.port}")
    private int port;
    @Autowired
    private LobbyRestController lobbyRestController;
    private String URL;

    private static final String SEND_SEND_MESSAGE_ENDPOINT = "/app/chat.sendMessage";
    private static final String SEND_ADD_USER_ENDPOINT = "/app/chat.addUser";
    private static final String SUBSCRIBE_TOPIC_ENDPOINT = "/topic/public";

    private CompletableFuture<ChatMessage> completableFutureChatMessage;

    @Test
    public void deleteThisTest(){

    }


    @Before
    public void setup() {
        completableFutureChatMessage = new CompletableFuture<>();
        URL = "ws://localhost:" + port + "/chat-socket";
    }

    //TODO Testen werken wel maar niet in deploy
  /*  @Test
    public void sendMessage() throws InterruptedException, ExecutionException, TimeoutException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe(SUBSCRIBE_TOPIC_ENDPOINT, new createMessageStompFrameHandler());
        stompSession.send(SEND_SEND_MESSAGE_ENDPOINT, new ChatMessage(ChatMessage.MessageType.CHAT, "test", "leonora", "1", "0"));
        ChatMessage chatMessage = completableFutureChatMessage.get(10, SECONDS);
        assertEquals("content found", "test", chatMessage.getContent());

    }

    @Test
    public void addUser() throws InterruptedException, ExecutionException, TimeoutException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe(SUBSCRIBE_TOPIC_ENDPOINT, new createMessageStompFrameHandler());
        stompSession.send(SEND_SEND_MESSAGE_ENDPOINT, new ChatMessage(ChatMessage.MessageType.JOIN, "", "leonora", "1", "0"));
        ChatMessage chatMessage = completableFutureChatMessage.get(10, SECONDS);
        assertEquals("joined", "JOIN", chatMessage.getType().toString());
    }


    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    private class createMessageStompFrameHandler implements StompFrameHandler {
        @Override
        @Nonnull
        public Type getPayloadType(@Nonnull StompHeaders stompHeaders) {
            return ChatMessage.class;
        }

        @Override
        public void handleFrame( @Nonnull StompHeaders stompHeaders, Object o) {
            completableFutureChatMessage.complete((ChatMessage) o);
        }
    }*/
}
