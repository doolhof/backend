package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.restControllers.LobbyRestController;
import be.kdg.backend.web.socketMessages.PartyClientMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import javax.annotation.Nonnull;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LobbyWebSocketControllerTest {

    @Test
    public void deleteThis() {

    }

    @Value("${local.server.port}")
    private int port;
    @Autowired
    private LobbyRestController lobbyRestController;

    @Autowired
    LobbyService lobbyService;

    @Autowired
    UserService userService;
    private String URL;

    private static final String SEND_JOIN_PARTY_ENDPOINT = "/party/join";
    private static final String SEND_LEAVE_PARTY_ENDPOINT = "/party/leave";
    private static final String SEND_LEAVE_HOST_PARTY_ENDPOINT = "/party/leaveHost";
    private static final String SUBSCRIBE_LOBBY_ENDPOINT = "/lobby/party";

    private CompletableFuture<Lobby> completableFutureLobby;
    private CompletableFuture<Boolean> completableFutureBoolean;

    @Before
    public void setup() {
        completableFutureLobby = new CompletableFuture<>();
        completableFutureBoolean = new CompletableFuture<>();
        URL = "ws://localhost:" + port + "/game-party-socket";
        lobbyRestController.createLobby(1, 4, 34);
    }

    @Test
    public void deleteThisTest() {

    }

    //TODO testen werken wel maar niet in deploy
  /*  @Test
    public void joinUser() throws InterruptedException, ExecutionException, TimeoutException, JsonProcessingException {
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        stompSession.subscribe(SUBSCRIBE_LOBBY_ENDPOINT, new CreateLobbyStompFrameHandler());
        stompSession.send(SEND_JOIN_PARTY_ENDPOINT, new PartyClientMessage("1", "1"));
        Lobby lobby = completableFutureLobby.get(10, SECONDS);
        assertEquals("admin found", "admin", lobby.getListUsers().stream().filter(x -> x.getUsername().equals("admin")).findFirst().get().getUsername());

    }
        @Test
        public void leaveUser () throws InterruptedException, ExecutionException, TimeoutException {
            lobbyRestController.createLobby(1, 4, 34);
            WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
            }).get(1, SECONDS);
            stompSession.subscribe(SUBSCRIBE_LOBBY_ENDPOINT, new CreateLobbyStompFrameHandler());
            Lobby lobby = lobbyService.findLobbyById(1);
            User admin = userService.findUserById(1);
            lobby.getListUsers().add(admin);
            stompSession.send(SEND_LEAVE_PARTY_ENDPOINT, new PartyClientMessage(admin.getUserId().toString(), "1"));
            Lobby newLobby = completableFutureLobby.get(10, SECONDS);
            assertNotEquals("not found", true, newLobby.getListUsers().stream().noneMatch(item -> "admin".equals(item.getUsername())));
        }

        @Test
        public void leaveHost () throws InterruptedException, ExecutionException, TimeoutException {
            WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            StompSession stompSession = stompClient.connect(URL, new StompSessionHandlerAdapter() {
            }).get(1, SECONDS);

            stompSession.subscribe(SUBSCRIBE_LOBBY_ENDPOINT, new CreateBooleanStompFrameHandler());
            stompSession.send(SEND_LEAVE_HOST_PARTY_ENDPOINT, new PartyClientMessage("1", "1"));
            Boolean result = completableFutureBoolean.get(10, SECONDS);
            assertTrue(result);
        }


        private List<Transport> createTransportClient () {
            List<Transport> transports = new ArrayList<>(1);
            transports.add(new WebSocketTransport(new StandardWebSocketClient()));
            return transports;
        }

        private class CreateLobbyStompFrameHandler implements StompFrameHandler {
            @Override
            @Nonnull
            public Type getPayloadType(@Nonnull StompHeaders stompHeaders) {
                return Lobby.class;
            }

            @Override
            public void handleFrame(@Nonnull StompHeaders stompHeaders, Object o) {
                completableFutureLobby.complete((Lobby) o);
            }
        }

        private class CreateBooleanStompFrameHandler implements StompFrameHandler {
            @Override
            @Nonnull
            public Type getPayloadType(@Nonnull StompHeaders stompHeaders) {
                return Boolean.class;
            }

            @Override
            public void handleFrame(@Nonnull StompHeaders stompHeaders, Object o) {
                completableFutureBoolean.complete((Boolean) o);
            }
        }*/
    }

