package be.kdg.backend.web;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class GameBoardRestController {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GameBoardService gameBoardService;

    @Test
    public void testLoadGameBoardWithAttributes() throws Exception {
        GameBoard gameBoard = gameBoardService.findGameBoard(1);
        this.mockMvc.perform(get("/api/gameboard/" + gameBoard.getGameBoardId()))
                .andDo(print())
                .andExpect(jsonPath("gameBoardId", is(gameBoard.getGameBoardId())))
                .andExpect(status().isOk());
    }
}
