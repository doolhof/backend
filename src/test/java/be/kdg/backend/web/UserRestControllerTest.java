package be.kdg.backend.web;

import be.kdg.backend.domain.user.*;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.exceptions.UserServiceException;
import be.kdg.backend.services.game.interfaces.RegistrationTokenService;
import be.kdg.backend.web.dto.PasswordDto;
import be.kdg.backend.web.dto.UserDto;
import be.kdg.backend.services.user.interfaces.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class UserRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private RegistrationTokenService registrationTokenService;

    private int userIdJos;

    @Before
    public void setUp() throws Exception {
        User user = userService.saveUser(new User("Jos","Jos@jos.be","jos",
                null, null, null,null, true, null, null));
      this.userIdJos =  user.getUserId();
    }





    @Test
    public void createUser() throws Exception {
        UserDto userDTO = new UserDto();
        userDTO.setUsername("root");
        userDTO.setEncryptedPassword("root");
        userDTO.setEmail("root@root.com");


        String postJSON = objectMapper.writeValueAsString(userDTO);
        mockMvc.perform(post("/api/users/sign-up/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJSON))
                .andDo(print());
    }

    @Test
    public void readUserByEmail() throws Exception {
        List<User> users = userService.findAllUsers();

        mockMvc.perform(get("/api/users/email/" + users.get(0).getEmail()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("userId", is(users.get(0).getUserId())))
                .andExpect(jsonPath("username", is(users.get(0).getUsername())));
    }


    @Test
    public void registerAndSignIn() {
        User Joe = new User();
        Joe.setUsername("Joe");
        Joe.setEncryptedPassword("joe");
        Joe.setEmail("Joe@Joe.com");
        Joe.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_CLIENT)));
        userService.saveUser(Joe);
        String token = userService.signIn(Joe.getUsername(),"joe");
        Assert.assertTrue(token.length()>10);
    }

    @Test
    public void confirmRegistrationTest() throws Exception{
        RegistrationToken token = new RegistrationToken("fb069349-cb45-482a",userService.findUserByUsername("admin"));
        registrationTokenService.saveToken(token);

        mockMvc.perform(post("/api/users/confirm?token="+token.getToken()))
                .andDo(print())
                .andExpect(status().isOk());

        Assert.assertTrue(userService.findUserByUsername("admin").isConfirmed());
    }

    @Test
    public void updateUser() throws Exception {
        UserDto userDTO = new UserDto();
        User user = userService.findUserById(4);
        userDTO.setUserId(user.getUserId());
        userDTO.setUsername(user.getUsername());
        userDTO.setEncryptedPassword(user.getEncryptedPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setLobbies(null);
        userDTO.setUserSetting(new UserSetting(false,false,false,true,false));
        userDTO.setUserStatistic(user.getUserStatistic());


        String postJSON = objectMapper.writeValueAsString(userDTO);
        mockMvc.perform(put("/api/users/update/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJSON))
                .andDo(print());

    }

    @Test
    public void updatePassword() throws Exception {
        PasswordDto passwordDto = new PasswordDto();
        passwordDto.setOldPassword("root");
        passwordDto.setNewPassword("zeger");
        passwordDto.setUserNumber(4);

        String postJSON = objectMapper.writeValueAsString(passwordDto);
        mockMvc.perform(put("/api/users/updatePassword/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJSON))
                .andDo(print());

        User userUpdated = userService.findUserById(4);
        Assert.assertTrue(BCrypt.checkpw("zeger",userUpdated.getEncryptedPassword()));
    }

    @Test(expected = UserNotFoundException.class)
    public void deleteAccount() throws Exception {
        mockMvc.perform(delete("/api/users/deleteAccount/"+userIdJos)
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        userService.findUserById(userIdJos);
    }

    @Test
    public void updateFcmToken() throws Exception {
        User fcmUser = new User("rest","res@des.com","res",null,null,null,null,false,null,null);
        String fcmToken = "Fdeiojfoijqmdoij53";
        userService.saveUser(fcmUser);
        int usrId = fcmUser.getUserId();
        mockMvc.perform(post("/api/users/updateFcmToken?token="+fcmToken+"&id="+usrId)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print());
        Assert.assertEquals(fcmToken,userService.findUserById(usrId).getFcmToken());
    }

}
