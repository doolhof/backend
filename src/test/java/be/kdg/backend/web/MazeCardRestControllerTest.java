package be.kdg.backend.web;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.services.game.interfaces.MazeCardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.MazeCardDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class MazeCardRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    //TODO mag weg?
    @Test
    public void testUpdateRemainingMazecard()  throws Exception {
        MazeCardDto mazeCardDto = new MazeCardDto();
        mazeCardDto.setMazeCardId(1);
        String postJSON = objectMapper.writeValueAsString(mazeCardDto);


        mockMvc.perform(post("/api/mazecard/updateRemaining/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJSON))
                .andDo(print());
    }
}
