package be.kdg.backend.web;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.MazeCardDto;
import be.kdg.backend.web.dto.TileDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TileRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TileService tileService;

    @Autowired
    GameBoardService gameBoardService;

    @Test
    public void moveTile() throws Exception {
        GameBoard gameBoard = gameBoardService.findGameBoard(1);
        Tile tile = tileService.findTileByPosition(gameBoard,2);
        MazeCardDto mazeCardDto = new MazeCardDto(tile.getMazeCard().getMazeCardId(), 0, true, true, false, false, 1);

        TileDto tileDto = new TileDto(tile.getTileId(), tile.getPosition(), tile.isMoveable(), mazeCardDto);

        String postJSON = objectMapper.writeValueAsString(tileDto);
        mockMvc.perform(post("/api/tile/move/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postJSON))
                .andDo(print());
    }

}
