package be.kdg.backend.services;


import be.kdg.backend.domain.game.*;
import be.kdg.backend.persistence.interfaces.TreasureRepository;
import be.kdg.backend.services.game.generators.treasureGenerator.TreasureGenerator;
import be.kdg.backend.services.game.implementations.treasure.TreasureServiceImpl;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class TreasureServiceTest {

    @Mock
    TreasureRepository treasureRepository;

    @Mock
    TreasureGenerator treasureGenerator;

    @InjectMocks
    TreasureServiceImpl treasureService;

    @Before
    public void setUp() throws Exception {
        List<Treasure> treasures = new ArrayList<>();
        treasures.add(new Treasure("earth"));
        treasures.add(new Treasure("mars"));
        treasures.add(new Treasure("planet_4"));
        treasures.add(new Treasure("saturn"));
        treasures.add(new Treasure("sun"));
        treasures.add(new Treasure("moon"));
        treasures.add(new Treasure("planet_3"));
        treasures.add(new Treasure("planet"));
        treasures.add(new Treasure("planet_1"));
        treasures.add(new Treasure("black_hole"));
        treasures.add(new Treasure("planet_2"));
        treasures.add(new Treasure("supernova"));

        //random treasures (12)
        treasures.add(new Treasure("asteroid"));
        treasures.add(new Treasure("monster_1"));
        treasures.add(new Treasure("monster_2"));
        treasures.add(new Treasure("monster_3"));
        treasures.add(new Treasure("monster_4"));
        treasures.add(new Treasure("monster_5"));
        treasures.add(new Treasure("monster_6"));
        treasures.add(new Treasure("monster_7"));
        treasures.add(new Treasure("monster_8"));
        treasures.add(new Treasure("monster_9"));
        treasures.add(new Treasure("astronaut"));
        treasures.add(new Treasure("star"));

        Mockito.when(treasureRepository.findAll()).thenAnswer(i -> treasures.stream().collect(Collectors.toList()));
    }

    @Test
    public void testFindAllTreasures() {
        List<Treasure> treasures =  treasureService.findAllTreasures();
        System.out.println(treasures.get(0).getName());
        assertNotEquals(treasures,null);
    }

    @Test
    public void testGenerateTreasures() {
        List<Treasure> treasures =  treasureService.generateTreasures();
        assertNotEquals(treasures,null);
    }

    @Test
    public void testUniqueTreasures() {
        List<Treasure> treasures =  treasureService.generateTreasures();
        boolean isUnique = treasures.stream().distinct().count() == treasures.stream().count();
        Assert.assertTrue("Every treasure is unique", isUnique);
    }


}
