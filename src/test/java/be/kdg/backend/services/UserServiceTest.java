package be.kdg.backend.services;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.domain.user.UserSetting;
import be.kdg.backend.persistence.interfaces.UserRepository;
import be.kdg.backend.security.JwtTokenProvider;
import be.kdg.backend.services.user.implementations.user.UserServiceImpl;
import be.kdg.backend.services.user.interfaces.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    UserServiceImpl userService;

    private User user1;
    private User user2;
    private User user3;


    @Before
    public void setUp() throws Exception {
        initializeMocks();
    }


    public void initializeMocks() {
        List<User> userList = new ArrayList<>();
        this.user1 = new User();
        user1.setUserId(1);
        user1.setUsername("user1");
        Mockito.when(bCryptPasswordEncoder.encode(Mockito.any(String.class))).thenCallRealMethod();
        user1.setEncryptedPassword(bCryptPasswordEncoder.encode("oudWachtwoord"));
        this.user2 = new User();
        user2.setUserId(2);
        user2.setUsername("user2");
        this.user3 = new User();
        user3.setUserId(3);
        user3.setUsername("user3");




        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        Mockito.when(userRepository.findByUsername(any(String.class))).thenAnswer(i -> java.util.Optional.ofNullable(userList.stream().filter(u -> u.getUsername().equals(i.getArgument(0))).findFirst().get()));
    }


    @Test
    public void testCheckUserNotExists() throws Exception {
        User user = userService.findUserByUsername("user1");
        Assert.assertTrue(userService.checkUserNotExist(user));
        try {
            User user2 = userService.findUserByUsername("user4");
            fail("The user is present");
        } catch (NoSuchElementException e) {

        }
    }


    @Test
    public void testUpdateUserSetting()  {
        User userToUpdate = userService.findUserByUsername("user1");
        userToUpdate.setUserSetting(new UserSetting(false,false,false,false,false));
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(userToUpdate);
        Mockito.when(userRepository.findById(Mockito.any(Integer.class))).thenReturn(java.util.Optional.ofNullable(userToUpdate));
        User updatedUser = userService.updateUser(userToUpdate);
        Assert.assertFalse(updatedUser.getUserSetting().isReceiveInvite());
    }


    @Test
    public void testCreateNewUser(){
        User user = new User();
        user.setUsername("zeger");
        user.setEncryptedPassword("zeg");
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        User userCreated = userService.saveUser(user);
        assertThat(userCreated.getUsername()).isSameAs(user.getUsername());
    }


    @Test
    public void testUpdatePassword(){
        User user = userService.findUserByUsername("user1");
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.when(userRepository.findById(Mockito.any(Integer.class))).thenReturn(java.util.Optional.ofNullable(user));
        userService.updatePassword(user.getUserId(),"oudWachtwoord","Jos");
        User userUpdated = userService.findUserByUsername(user.getUsername());
        assertThat(BCrypt.checkpw("Jos",userUpdated.getEncryptedPassword()));
    }

}
