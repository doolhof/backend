package be.kdg.backend.services;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.services.user.interfaces.EmailService;
import be.kdg.backend.services.user.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;


    private MimeMessage mimeMessage;

    @Before
    public void before() {
        mimeMessage = new MimeMessage((Session) null);
    }

    @Test
    public void test() {

    }

 /*   @Test
    public void sendMailRegistrationTest() throws Exception {
        User user = userService.findUserByUsername("root");
        List<User> users = new ArrayList<>();
        users.add(user);
          mimeMessage = emailService.sendSimpleMessage("Registration", "You are registered", users, 0);
         assertEquals(user.getEmail(), mimeMessage.getRecipients(MimeMessage.RecipientType.TO)[0].toString());
        assertEquals("Registration", mimeMessage.getSubject());
    }

    @Test
    public void sendInvitationTest() throws MessagingException {
        User root = userService.findUserByUsername("root");
        User admin = userService.findUserByUsername("Jan");
        List<User> users = new ArrayList<>();
        users.add(root);
        users.add(admin);
        mimeMessage = emailService.sendSimpleMessage("Invitation", "I want to invite you!", users, 1);
        assertEquals(root.getEmail(), mimeMessage.getRecipients(MimeMessage.RecipientType.TO)[0].toString());
        assertEquals(admin.getEmail(), mimeMessage.getRecipients(MimeMessage.RecipientType.TO)[1].toString());
        assertEquals("Invitation", mimeMessage.getSubject());
    }*/
}
