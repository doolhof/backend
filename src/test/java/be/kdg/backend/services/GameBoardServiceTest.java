package be.kdg.backend.services;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.persistence.interfaces.GameBoardRepository;
import be.kdg.backend.persistence.interfaces.TileRepository;
import be.kdg.backend.services.game.generators.mazeCardGenerator.MazeCardGenerator;
import be.kdg.backend.services.game.generators.mazeCardGenerator.MazeCardGeneratorHelper;
import be.kdg.backend.services.game.generators.tileGenerator.TileGenerator;
import be.kdg.backend.services.game.generators.treasureGenerator.TreasureGenerator;
import be.kdg.backend.services.game.implementations.gameBoard.GameBoardServiceImpl;
import be.kdg.backend.services.game.implementations.tile.TileServiceImpl;
import be.kdg.backend.services.game.implementations.treasure.TreasureServiceImpl;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.services.game.interfaces.TreasureService;
import be.kdg.backend.web.dto.TileDto;
import org.checkerframework.checker.units.qual.A;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameBoardServiceTest {

    @Autowired
    GameBoardService gameBoardService;

    @Autowired
    TreasureService treasureService;
    /**
     * Note this is 50 because the remaining mazecard gets tile position 50, but this is a "hidden" tile
     */
    @Test
    public void testGameboardSize() {
        GameBoard gameBoard = gameBoardService.createGameBoard(treasureService.generateTreasures());
        assertEquals(gameBoard.getTiles().size(), 50);
    }

    @Test
    public void testTileHasMazeCard() {
        GameBoard gameBoard = gameBoardService.createGameBoard(treasureService.generateTreasures());
        for (int i = 0; i < gameBoard.getTiles().size(); i++) {
            assertNotNull(gameBoard.getTiles().get(i).getMazeCard());
        }
    }

    @Test
    public void testFixedPositionTiles() {
        GameBoard gameBoard = gameBoardService.createGameBoard(treasureService.generateTreasures());
        for (int i = 0; i < 7; i = i + 2) {
            for (int x = i; x < 49; x = x + 14) {
                assertEquals(gameBoard.getTiles().get(x).isMoveable(), false);
            }
        }
    }

    @Test
    public void testFixedPositionTilesMazeCard() {
        GameBoard gameBoard = gameBoardService.createGameBoard(treasureService.generateTreasures());
        for (int i = 0; i < 7; i = i + 2) {
            for (int x = i; x < 49; x = x + 14) {
                assertNotNull(gameBoard.getTiles().get(x).getMazeCard());
            }

        }
    }
}
