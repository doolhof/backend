package be.kdg.backend.services;

import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.LobbyNotFoundException;
import be.kdg.backend.exceptions.LobbyServiceException;
import be.kdg.backend.persistence.interfaces.LobbyRepository;
import be.kdg.backend.services.user.implementations.lobby.LobbyServiceImpl;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.services.user.interfaces.LobbyService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LobbyServiceTest {
    @InjectMocks
    LobbyServiceImpl lobbyService;

    @Mock
    LobbyRepository lobbyRepository;

    @Mock
    UserService userService;


    private Lobby lobby;

    @Before
    public void setUp() throws Exception {
        List<User> users = new ArrayList<>();

        User user = new User();
        user.setUserId(1);
        user.setUsername("zeger");

        User user2 = new User();
        user2.setUserId(2);
        user2.setUsername("Jos");

        users.add(user);
        users.add(user2);

        Lobby lobby = new Lobby();
        lobby.setLobbyId(1);
        lobby.setListUsers(users);
        this.lobby = lobby;

        Mockito.when(lobbyRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(lobby));

    }

    @Test
    public void testCreationLobby(){
        Mockito.when(lobbyRepository.save(any(Lobby.class))).thenAnswer(i -> i.getArgument(0));
        Lobby lobbyCreated = lobbyService.saveLobby(lobby);
        assertNotNull(lobbyCreated.getListUsers());
        assertEquals(1,lobbyCreated.getLobbyId());
    }

    @Test
    public void testGetExistingLobby(){
        Lobby lobby = new Lobby();
        lobby.setLobbyId(1);
        assertNotNull(lobbyService.findLobbyById(1));
    }


    @Test(expected = LobbyNotFoundException.class)
    public void testgetNonExistingLobby(){
        lobbyService.findLobbyById(5);
    }


}
