package be.kdg.backend.services;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.domain.game.Treasure;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.GameBoardRepository;
import be.kdg.backend.persistence.interfaces.TileRepository;
import be.kdg.backend.services.game.generators.mazeCardGenerator.MazeCardGenerator;
import be.kdg.backend.services.game.generators.mazeCardGenerator.MazeCardGeneratorHelper;
import be.kdg.backend.services.game.generators.tileGenerator.TileGenerator;
import be.kdg.backend.services.game.generators.treasureGenerator.TreasureGenerator;
import be.kdg.backend.services.game.implementations.gameBoard.GameBoardServiceImpl;
import be.kdg.backend.services.game.implementations.tile.TileServiceImpl;
import be.kdg.backend.services.game.implementations.treasure.TreasureServiceImpl;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.checkerframework.checker.units.qual.A;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.refEq;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TileServiceTest {
    @Mock
    GameBoardService gameBoardService;

    @Mock
    TileRepository tileRepository;

    @Mock
    GameBoardRepository gameBoardRepository;

    @Autowired
    TileGenerator tileGenerator;

    @Autowired
    TreasureGenerator treasureGenerator;

    TileService tileService;

    GameBoard gameBoard;

    @Before
    public void setUp() {
        GameBoard gameBoard = new GameBoard();
        gameBoard.setGameBoardId(1);
        List<Tile> tiles = tileGenerator.generateTiles(gameBoard, treasureGenerator.generateTreasures());
        for (int i = 0; i < tiles.size() ; i++) {
            tiles.get(i).setTileId(i);
        }
        gameBoard.setTiles(tiles);
        tileService = new TileServiceImpl(tileRepository,gameBoardService);
        Mockito.when(gameBoardService.findGameBoard(1)).thenReturn(gameBoard);
        Mockito.when(gameBoardRepository.save(Mockito.any(GameBoard.class))).thenReturn(gameBoard);
        this.gameBoard = gameBoard;
    }

    @Test
    @Transactional
    public void testOneMazeCardWithoutTile(){
        assertNotEquals(tileService.findMazeCardWithoutTile(gameBoard.getGameBoardId()),null);
    }



    @Test
    public void testTileHasMazeCard() {
        Mockito.when(tileRepository.findById(Mockito.any(Integer.class)))
                .thenReturn(java.util.Optional.ofNullable(gameBoard.getTiles().get(3)));
        assertNotEquals(tileService.findTileById(3).getMazeCard(), null);
    }

    @Test
    public void testMazeCardFixedTileNotMoveable() {
        Mockito.when(tileRepository.findById(Mockito.any(Integer.class)))
                .thenReturn(java.util.Optional.ofNullable(gameBoard.getTiles().get(0)));
        assertThat(tileService.findTileById(0).isMoveable(), is(false));
    }

    @Test
    public void testMazeCardPush() {
        Mockito.when(tileRepository.findTileByGameBoardAndPosition(Mockito.any(GameBoard.class),
                Mockito.any(Integer.class))).thenReturn(gameBoard.getTiles().get(14));
        int position = 14;
        MazeCard mazecard = tileService.findMazeCardWithoutTile(1);
        tileService.pushMazeCard(position, 1);
        assertEquals(mazecard.getMazeCardId(), tileService.findTileByPosition(gameBoard, 14)
                .getMazeCard().getMazeCardId());
    }

    @Test
    public void testMazeCardPushTop() {
        Mockito.when(tileRepository.findTileByGameBoardAndPosition(Mockito.any(GameBoard.class),
                Mockito.any(Integer.class))).thenReturn(gameBoard.getTiles().get(14));
        int position = 2;
        Tile tile = tileService.findTileByPosition(gameBoard, position);
        tileService.pushMazeCard(position, 1);
        assertEquals(tile.getMazeCard().getMazeCardId(), tileService.findTileByPosition(gameBoard, 9).getMazeCard().getMazeCardId());
    }

    @Test
    public void testMazeCardPushDown() {
        Mockito.when(tileRepository.findTileByGameBoardAndPosition(Mockito.any(GameBoard.class),
                Mockito.any(Integer.class))).thenReturn(gameBoard.getTiles().get(14));
        int position = 44;
        Tile tile = tileService.findTileByPosition(gameBoard, 44);
        tileService.pushMazeCard(position, 1);
        assertEquals(tile.getMazeCard().getMazeCardId(), tileService.findTileByPosition(gameBoard, 37).getMazeCard().getMazeCardId());
    }

    @Test
    public void testMazeCardPushLeft() {
        Mockito.when(tileRepository.findTileByGameBoardAndPosition(Mockito.any(GameBoard.class),
                Mockito.any(Integer.class))).thenReturn(gameBoard.getTiles().get(14));
        int position = 8;
        Tile tile = tileService.findTileByPosition(gameBoard, 8);
        tileService.pushMazeCard(position, 1);
        assertEquals(tile.getMazeCard().getMazeCardId(), tileService.findTileByPosition(gameBoard, 9).getMazeCard().getMazeCardId());
    }

    @Test
    public void testMazeCardPushRight() {
        Mockito.when(tileRepository.findTileByGameBoardAndPosition(Mockito.any(GameBoard.class),
                Mockito.any(Integer.class))).thenReturn(gameBoard.getTiles().get(14));
        int position = 14;
        Tile tile = tileService.findTileByPosition(gameBoard, 14);
        tileService.pushMazeCard(position, 1);
        assertEquals(tile.getMazeCard().getMazeCardId(), tileService.findTileByPosition(gameBoard, 13).getMazeCard().getMazeCardId());
   }


}
