package be.kdg.backend.services;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.PlayerRepository;
import be.kdg.backend.services.game.implementations.player.PlayerMoveTileHelper;
import be.kdg.backend.services.game.implementations.player.PlayerServiceImpl;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import org.aspectj.apache.bcel.util.Play;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class PlayerServiceImplTest {
    @Mock
    PlayerRepository playerRepository;
    @Mock
    PlayerMoveTileHelper playerMoveTileHelper;

    @InjectMocks
    PlayerServiceImpl playerService;

    private Player player;

    @Before
    public void setUp() throws Exception {
        List<Player> players = new ArrayList<>();
        Game game = new Game();
        this.player = new Player();
        player.setName("Zeger");
        player.setPlayerId(1);
        player.setGame(game);
        players.add(player);

        game.setPlayerList(players);
        Mockito.when(playerRepository.findById(Mockito.any(Integer.class))).thenReturn(java.util.Optional.ofNullable(player));
        // Mockito.when(playerRepository.findAll()).thenAnswer(i -> players.stream().collect(Collectors.toList()));

    }

    @Test
    public void movePlayerFromEdge() {
        // Mockito.when(playerRepository.save(Mockito.any(Player.class))).thenReturn(player);
        Mockito.when(playerMoveTileHelper.moveTileWithPlayer(Mockito.any(Player.class),Mockito.any(Integer.class))).thenCallRealMethod();

        player.setCurrentPosition(8);
        playerService.savePushedPosition(player, 14);
        assertEquals(player.getCurrentPosition(), 14);


        player.setCurrentPosition(2);
        playerService.savePushedPosition(player, 44);
        assertEquals(player.getCurrentPosition(),44);

        player.setCurrentPosition(14);
        playerService.savePushedPosition(player, 8);
        assertEquals(player.getCurrentPosition(),8);

        player.setCurrentPosition(44);
        playerService.savePushedPosition(player, 2);
        assertEquals(player.getCurrentPosition(),2);
    }

    @Test
    public void pushPlayerOnTile() {
        // Mockito.when(playerRepository.save(Mockito.any(Player.class))).thenReturn(player);
        Mockito.when(playerMoveTileHelper.moveTileWithPlayer(Mockito.any(Player.class),Mockito.any(Integer.class))).thenCallRealMethod();

        Player player = playerService.getPlayerByUserId(1);

        player.setCurrentPosition(8);
        playerService.savePushedPosition(player, 8);
        assertEquals(player.getCurrentPosition(), 9);

        player.setCurrentPosition(2);
        playerService.savePushedPosition(player, 2);
        assertEquals(player.getCurrentPosition(),9);

        player.setCurrentPosition(14);
        playerService.savePushedPosition(player, 14);
        assertEquals(player.getCurrentPosition(),13);

        player.setCurrentPosition(44);
        playerService.savePushedPosition(player, 44);
        assertEquals(player.getCurrentPosition(),37);
    }

 /*   @Test
    public void move() {
        GameBoard gameBoard = gameBoardService.findGameBoard(1);
        Player player = playerService.getPlayerByUserId(1);
        int destination = 21;
        if (player.getCurrentPosition() == 21) {
            destination = 14;
        } else {
            destination = 21;
        }
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), destination);
        Assertions.assertThat(playerService.move(player, destinationTile))
                .hasSize(2);
    }*/
}