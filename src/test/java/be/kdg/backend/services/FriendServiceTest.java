package be.kdg.backend.services;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.UserRepository;
import be.kdg.backend.services.user.implementations.user.FriendServiceImpl;
import be.kdg.backend.services.user.interfaces.FriendService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class FriendServiceTest {
    @InjectMocks
    FriendServiceImpl friendService;

    @Mock
    UserRepository userRepository;

    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        List<User> userList = new ArrayList<>();
        this.user1 = new User();
        user1.setUserId(1);
        user1.setUsername("user1");
        user1.setEncryptedPassword("oudWachtwoord");
        List<User> friends1 = new ArrayList<>();
        user1.setFriends(friends1);
        this.user2 = new User();
        user2.setUserId(2);
        user2.setUsername("user2");
        List<User> friends2 = new ArrayList<>();
        user2.setFriends(friends2);
        this.user3 = new User();
        user3.setUserId(3);
        user3.setUsername("user3");

        user1.getFriends().add(user2);
        user2.getFriends().add(user1);

        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        Mockito.when(userRepository.findById(any(Integer.class))).thenAnswer(i -> java.util.Optional.ofNullable(userList.stream().filter(u -> u.getUserId().equals(i.getArgument(0))).findFirst().get()));
    }



    @Test
    public void testHasFriends() {
        List<User> users= friendService.getUserFriends(1);
        Assert.assertTrue(users.get(0).getUsername().equals(user2.getUsername()));
    }

    @Test
    public void testAddFriend() {
        Mockito.when(userRepository.save(any(User.class))).thenAnswer(i -> i.getArgument(0));
        friendService.addFriend(user2.getUserId(),user3.getUserId());
        Assert.assertTrue(user2.getFriends().contains(user3));
    }

    @Test
    public void testRemoveFriend() {
        Mockito.when(userRepository.save(any(User.class))).thenAnswer(i -> i.getArgument(0));
        friendService.removeFriend(user1.getUserId(),user2.getUserId());
        Assert.assertFalse(user1.getFriends().contains(user2));
    }
}
