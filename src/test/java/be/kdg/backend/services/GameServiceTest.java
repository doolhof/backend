package be.kdg.backend.services;


import be.kdg.backend.domain.game.*;
import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.GameNotFoundException;
import be.kdg.backend.persistence.interfaces.GameRepository;
import be.kdg.backend.services.game.generators.playerGenerator.PlayerGenerator;
import be.kdg.backend.services.game.implementations.game.GameServiceHelper;
import be.kdg.backend.services.game.implementations.game.GameServiceImpl;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class GameServiceTest {
    @InjectMocks
    GameServiceImpl gameService;

    @Mock
    GameRepository gameRepository;
    @Mock
    PlayerService playerService;
    @Mock
    GameBoardService gameBoardService;
    @Mock
    TreasureService treasureService;
    @Mock
    PlayerGenerator playerGenerator;
    @Mock
    GameServiceHelper gameServiceHelper;

    @Before
    public void setUp() throws Exception {

        List<Player> players = new ArrayList<>();

        Player player1 = new Player();
        player1.setName("Zeger");
        player1.setColor(Color.DODGERBLUE);
        player1.setPlayerId(1);

        Player player2 = new Player();
        player2.setName("Zeger");
        player2.setColor(Color.GREEN);
        player2.setPlayerId(2);

        players.add(player1);
        players.add(player2);

        GameBoard gameBoard = new GameBoard();
        gameBoard.setGameBoardId(1);
        gameBoard.setTiles(null);


        Game game = new Game(LocalDateTime.now(), null, GameStatus.WAITING, gameBoard, players, 30);
        Mockito.when(gameRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(game));

    }

    @Test
    public void startGame() {
        List<User> users = new ArrayList<>();
        User user1 = new User();
        user1.setUserId(1);
        user1.setUsername("Zeger");
        User user2 = new User();
        user2.setUserId(2);
        user2.setUsername("Mark");
        users.add(user1);
        users.add(user2);
        GameSetting gameSetting = new GameSetting(users.size(), 30);


        Lobby lobby = new Lobby(users, gameSetting);
        Player player = new Player();
        Mockito.when(treasureService.generateTreasures()).thenReturn(generateTreasures());
        Mockito.when(gameBoardService.createGameBoard(Mockito.any())).thenReturn(createGameboard());
        Mockito.when(playerService.createPlayer(any(Color.class), any(String.class), any(), any(Game.class)))
                .thenReturn(player);
        Mockito.when(playerService.saveAllPlayers(any())).thenAnswer(i -> i.getArgument(0));

        Game game = gameService.startGame(new Game(), lobby.getListUsers(), lobby.getGameSettings().getSecondsPerTurn());

        Assert.assertNotNull(game);
    }


    @Test
    public void CreateNewGame() {
        Mockito.when(gameRepository.save(any(Game.class))).thenAnswer(i -> i.getArgument(0));
        Game game = gameService.getGameById(1);
        Game savedGame = gameService.saveGame(game);
        assertEquals(savedGame.getPlayerList().size(), 2);
    }

    @Test
    public void testGetExistingGame() {
        assertNotNull(gameService.getGameById(1));
    }

    @Test(expected = GameNotFoundException.class)
    public void testGetNonExistingGame() {
        assertNotNull(gameService.getGameById(2));
    }


    private GameBoard createGameboard() {
        GameBoard gameBoard = new GameBoard();
        gameBoard.setGameBoardId(1);
        return gameBoard;
    }

    private List<Treasure> generateTreasures() {
        List<Treasure> treasures = new ArrayList<>();
        treasures.add(new Treasure("earth"));
        treasures.add(new Treasure("mars"));
        treasures.add(new Treasure("planet_4"));
        treasures.add(new Treasure("saturn"));
        treasures.add(new Treasure("sun"));
        treasures.add(new Treasure("moon"));
        treasures.add(new Treasure("planet_3"));
        treasures.add(new Treasure("planet"));
        treasures.add(new Treasure("planet_1"));
        treasures.add(new Treasure("black_hole"));
        treasures.add(new Treasure("planet_2"));
        treasures.add(new Treasure("supernova"));

        //random treasures (12)
        treasures.add(new Treasure("asteroid"));
        treasures.add(new Treasure("monster_1"));
        treasures.add(new Treasure("monster_2"));
        treasures.add(new Treasure("monster_3"));
        treasures.add(new Treasure("monster_4"));
        treasures.add(new Treasure("monster_5"));
        treasures.add(new Treasure("monster_6"));
        treasures.add(new Treasure("monster_7"));
        treasures.add(new Treasure("monster_8"));
        treasures.add(new Treasure("monster_9"));
        treasures.add(new Treasure("astronaut"));
        treasures.add(new Treasure("star"));
        return treasures;
    }
}
