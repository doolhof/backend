package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.PlayerDto;
import be.kdg.backend.web.dto.TileDto;
import be.kdg.backend.web.socketMessages.PlayerClientMessage;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@Transactional
public class PlayerWebSocketController {
    private final PlayerService playerService;
    private final ModelMapper modelMapper;
    private final TileService tileService;
    private final GameService gameService;

    public PlayerWebSocketController(PlayerService playerService, ModelMapper modelMapper, TileService tileService, GameService gameService) {
        this.playerService = playerService;
        this.modelMapper = modelMapper;
        this.tileService = tileService;
        this.gameService = gameService;
    }

    @MessageMapping("/move")
    @SendTo("/game/movePawn")
    public List<TileDto> move(PlayerClientMessage playerClientMessage) throws InterruptedException {
        Thread.sleep(300);
        Player player = playerService.getPlayerByUserId(Integer.parseInt(playerClientMessage.getPlayerId()));
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), Integer.parseInt(playerClientMessage.getDestination()));
        LinkedList<Tile> path = playerService.move(player, destinationTile);

        if (path.isEmpty()) {
            return null;
        } else {
            return path.stream()
                    .map(element -> modelMapper.map(element, TileDto.class))
                    .collect(Collectors.toList());
        }
    }

    @MessageMapping("/getPlayer")
    @SendTo("/game/getPlayer")
    public PlayerDto getPlayerById(String playerId) throws InterruptedException {
        Thread.sleep(300);
        return modelMapper.map(playerService.getPlayerByUserId(Integer.parseInt(playerId)), PlayerDto.class);
    }

    @MessageMapping("/treasureSearch")
    @SendTo("/game/gameParty")
    public void treasureSearch(PlayerClientMessage playerClientMessage) throws InterruptedException {
        Player player = playerService.getPlayerByUserId(Integer.parseInt(playerClientMessage.getPlayerId()));
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), Integer.parseInt(playerClientMessage.getDestination()));
        playerService.updatePlayerTreasure(destinationTile, player);
    }

    @MessageMapping("/movePlayerTile")
    @SendTo("/game/movePlayerTile")
    public List<PlayerDto> movePlayerTile(PlayerClientMessage playerClientMessage) throws InterruptedException {
        Player player = playerService.getPlayerByUserId(Integer.parseInt(playerClientMessage.getPlayerId()));
        List<PlayerDto> playerDtos =
         playerService.savePushedPosition(player, Integer.parseInt(playerClientMessage.getDestination())).stream()
                .map(element -> modelMapper.map(element, PlayerDto.class))
                .collect(Collectors.toList());
        return playerDtos;
    }

    @MessageMapping("/checkWinGame")
    @SendTo("/game/checkWinGame")
    public String checkVictory(String playerId) throws InterruptedException {
        Thread.sleep(300);
        Player player = playerService.getPlayerByUserId(Integer.parseInt(playerId));
        boolean won = playerService.winGame(player);
        if (won == true) {
            return player.getName();
        }
        return null;
    }
}
