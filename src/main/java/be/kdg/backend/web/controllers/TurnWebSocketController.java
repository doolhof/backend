package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.game.Turn;
import be.kdg.backend.services.game.interfaces.TurnService;
import be.kdg.backend.web.dto.TurnDto;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Transactional
public class TurnWebSocketController {
    private final TurnService turnService;
    private final ModelMapper modelMapper;

    public TurnWebSocketController(TurnService turnService, ModelMapper modelMapper) {
        this.turnService = turnService;
        this.modelMapper = modelMapper;
    }

    @MessageMapping("/endTurn")
    @SendTo("/game/endTurn")
    public Boolean endTurn(TurnDto turnDto) {
        turnService.endTurn(
                turnDto.getTurnDuration(),
                turnDto.getTurnPhase().toString(),
                turnDto.getPosMazeCardMove(),
                turnDto.getPlayerMoves(),
                turnDto.getPlayer(),
                turnDto.getOrientation(),
                turnDto.getPlayerName());
        return true;
        // return modelMapper.map(turn, TurnDto.class);
    }

//    @MessageMapping("/getTurns")
//    @SendTo("/game/turn")
//    public List<TurnDto> getTurns(String gameId){
//        List<Turn> turns = turnService.getAllTurnsByGameId(Integer.parseInt(gameId));
//        List<TurnDto> turnDtos = turns.stream().map(element -> modelMapper.map(element, TurnDto.class)).collect(Collectors.toList());
//        return turnDtos;
//    }
}
