package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.exceptions.GameNotFoundException;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.dto.GameDto;
import be.kdg.backend.web.socketMessages.GameClientMessage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
@Transactional
public class GameWebSocketController {
    private final GameService gameService;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public GameWebSocketController(GameService gameService, UserService userService, ModelMapper modelMapper) {
        this.gameService = gameService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    // gameParty/getGame
    @MessageMapping("/getGame.joinGame")
    @SendTo("/game/gameParty")
    public GameDto joinGame(GameClientMessage gameClientMessage) throws GameNotFoundException {
         Game game = gameService.getGameById(Integer.parseInt(gameClientMessage.getGameId()));
         return modelMapper.map(game, GameDto.class);
    }
}
