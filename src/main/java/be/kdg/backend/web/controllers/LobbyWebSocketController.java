package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;

import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.socketMessages.PartyClientMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
@Transactional
public class LobbyWebSocketController {
    private final LobbyService lobbyService;
    private final UserService userService;

    @Autowired
    public LobbyWebSocketController(LobbyService lobbyService, UserService userService) {
        this.lobbyService = lobbyService;
        this.userService = userService;
    }

    @MessageMapping("/join")
    @SendTo("/lobby/party")
    public Lobby joinUser(PartyClientMessage partyClientMessage) throws Exception {
        Thread.sleep(200);
        Lobby lobby = lobbyService.findLobbyById(Integer.parseInt(partyClientMessage.getLobbyId()));
        User user = userService.findUserById(Integer.parseInt(partyClientMessage.getUserId()));
        if (lobby.getListUsers().stream().noneMatch(o -> o.getUserId().equals(user.getUserId()))
                && lobby.getListUsers().size() - 1 < lobby.getGameSettings().getAmountOfUsers()) {
            user.getLobbies().add(lobby);
            lobby.getListUsers().add(user);
            lobby = lobbyService.saveLobby(lobby);
            userService.saveUser(user);
        }
        return lobby;
    }

    @MessageMapping("/leave")
    @SendTo("/lobby/party")
    public Lobby leaveUser(PartyClientMessage partyClientMessage) throws Exception {
        Thread.sleep(200);
        Lobby lobby = lobbyService.findLobbyById(Integer.parseInt(partyClientMessage.getLobbyId()));
        User user = userService.findUserById(Integer.parseInt(partyClientMessage.getUserId()));
        if (lobby.getListUsers().stream().anyMatch(o -> o.getUserId().equals(user.getUserId()))) {
            user.getLobbies().remove(lobby);
            lobby.getListUsers().remove(user);
            lobby = lobbyService.saveLobby(lobby);
            userService.saveUser(user);
        }
        return lobby;
    }

    @MessageMapping("/leaveHost")
    @SendTo("/lobby/party")
    public boolean leaveHost(PartyClientMessage partyClientMessage) {
        Lobby lobby = lobbyService.findLobbyById(Integer.parseInt(partyClientMessage.getLobbyId()));
        if (lobby.getHost().getUserId() == (Integer.parseInt(partyClientMessage.getUserId()))) {
            if (lobby.getListUsers() != null) lobby.getListUsers().clear();
            lobbyService.deleteLobby(lobby);
            return true;
        }
        return false;
    }

    @MessageMapping("/startGame")
    @SendTo("/lobby/party")
    public String startGame(PartyClientMessage partyClientMessage) {
        Lobby lobby = lobbyService.findLobbyById(Integer.parseInt(partyClientMessage.getLobbyId()));
        if (lobby.getHost().getUserId() == (Integer.parseInt(partyClientMessage.getUserId()))) {
            // TODO: 18-Mar-19 Set gameStatus to playing
            return "start:" + lobby.getGame().getGameId();
        }
        return "nostart";
    }
}
