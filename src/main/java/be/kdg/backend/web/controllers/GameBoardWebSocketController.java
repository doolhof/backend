package be.kdg.backend.web.controllers;

import be.kdg.backend.services.game.interfaces.GameBoardService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
@Transactional
public class GameBoardWebSocketController {
    private final GameBoardService gameBoardService;

    public GameBoardWebSocketController(GameBoardService gameBoardService) {
        this.gameBoardService = gameBoardService;
    }

    @MessageMapping("/disabledArrow")
    @SendTo("/game/disabledArrow")
    public int getDisabledArrow(String number) {
        return gameBoardService.getDisabledArrow(Integer.parseInt(number));
    }
}
