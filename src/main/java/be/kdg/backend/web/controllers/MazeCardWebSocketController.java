package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.game.interfaces.MazeCardService;
import be.kdg.backend.web.dto.MazeCardDto;
import org.modelmapper.ModelMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
@Transactional
public class MazeCardWebSocketController {
    private final MazeCardService mazeCardService;
    private final GameService gameService;
    private final ModelMapper modelMapper;

    public MazeCardWebSocketController(MazeCardService mazeCardService, GameService gameService, ModelMapper modelMapper) {
        this.mazeCardService = mazeCardService;
        this.gameService = gameService;
        this.modelMapper = modelMapper;
    }

    @MessageMapping("/updateRemaining")
    @SendTo("/game/remaining")
    public MazeCardDto updateRemainingMazeCard(MazeCardDto mazeCardDto) throws InterruptedException {
        MazeCard mazeCard = mazeCardService.findMazeCardById(mazeCardDto.getMazeCardId());
        mazeCard = mazeCardService.changeOrientation(mazeCard, mazeCardDto.getOrientation());
        return modelMapper.map( mazeCardService.saveMazeCard(mazeCard), MazeCardDto.class);
    }
}
