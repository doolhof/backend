package be.kdg.backend.web.controllers;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.GameBoardDto;
import be.kdg.backend.web.socketMessages.TileClientMessage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

@Controller
@Transactional
public class TileWebSocketController {
    private final TileService tileService;
    private final GameBoardService gameBoardService;
    private final ModelMapper modelMapper;

    @Autowired
    public TileWebSocketController(TileService tileService, GameBoardService gameBoardService, ModelMapper modelMapper) {
        this.tileService = tileService;
        this.gameBoardService = gameBoardService;
        this.modelMapper = modelMapper;
    }

    @MessageMapping("/moveMazeCard")
    @SendTo("/game/moveMazeCard")
    public GameBoardDto moveMazeCard(TileClientMessage tileClientMessage) {
        tileService.pushMazeCard(Integer.parseInt(tileClientMessage.getPosition()), Integer.parseInt(tileClientMessage.getGameBoardId()));
        GameBoard gameBoard = gameBoardService.findGameBoard(Integer.parseInt(tileClientMessage.getGameBoardId()));
        return modelMapper.map(gameBoard, GameBoardDto.class);
    }
}
