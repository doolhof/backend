package be.kdg.backend.web.socketMessages;

public class PartyClientMessage {
    private String userId;
    private String lobbyId;

    public PartyClientMessage() {
    }

    public PartyClientMessage(String userId, String lobbyId) {
        this.userId = userId;
        this.lobbyId = lobbyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(String lobbyId) {
        this.lobbyId = lobbyId;
    }
}
