package be.kdg.backend.web.socketMessages;

public class PlayerClientMessage {
    private String playerId;
    private String destination;

    public PlayerClientMessage() {
    }

    public PlayerClientMessage(String playerId, String destination) {
        this.playerId = playerId;
        this.destination = destination;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
