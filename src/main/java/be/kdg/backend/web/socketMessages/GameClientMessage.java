package be.kdg.backend.web.socketMessages;

public class GameClientMessage {
    private String gameId;
    private String userId;

    public GameClientMessage() {
    }

    public GameClientMessage(String gameId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
