package be.kdg.backend.web.socketMessages;

public class TileClientMessage {
    private String position;
    private String gameBoardId;

    public TileClientMessage() {
    }

    public TileClientMessage(String position, String gameBoardId) {
        this.position = position;
        this.gameBoardId = gameBoardId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getGameBoardId() {
        return gameBoardId;
    }

    public void setGameBoardId(String gameBoardId) {
        this.gameBoardId = gameBoardId;
    }
}
