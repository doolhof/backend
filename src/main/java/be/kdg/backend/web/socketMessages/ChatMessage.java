package be.kdg.backend.web.socketMessages;

public class ChatMessage {

    private MessageType type;
    private String content;
    private String sender;
    private String lobbyId;
    private String gameId;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

    public ChatMessage(MessageType type, String content, String sender, String lobbyId, String gameId) {
        this.type = type;
        this.content = content;
        this.sender = sender;
        this.lobbyId = lobbyId;
        this.gameId = gameId;
    }

    public ChatMessage() {
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(String lobbyId) {
        this.lobbyId = lobbyId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
