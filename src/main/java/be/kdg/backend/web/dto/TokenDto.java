package be.kdg.backend.web.dto;

import be.kdg.backend.domain.user.User;

public class TokenDto {
    private String token;
    public TokenDto() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
