package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.Color;
import be.kdg.backend.domain.game.Turn;

import java.util.List;


public class PlayerDto {

    private int playerId;
    private Color color;
    private String name;
    private List<TreasureDto> toFind;
    private List<TreasureDto> found;
    private List<TurnDto> turns;
    private int currentPosition;
    private int startPosition;
    private boolean isTurn;

    public PlayerDto(int id, Color color, int startPosition, int currentPosition, List<TreasureDto> toFind, List<TreasureDto> found, List<TurnDto> turns, boolean isTurn, String name) {
        this.playerId = id;
        this.color = color;
        this.name = name;
        this.toFind = toFind;
        this.found = found;
        this.turns = turns;
        this.currentPosition = currentPosition;
        this.startPosition = startPosition;
        this.isTurn = isTurn;
    }

    public PlayerDto() {
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public List<TreasureDto> getToFind() {
        return toFind;
    }

    public void setToFind(List<TreasureDto> toFind) {
        this.toFind = toFind;
    }

    public List<TreasureDto> getFound() {
        return found;
    }

    public void setFound(List<TreasureDto> found) {
        this.found = found;
    }

    public List<TurnDto> getTurns() {
        return turns;
    }

    public void setTurns(List<TurnDto> turns) {
        this.turns = turns;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public boolean isTurn() {
        return isTurn;
    }

    public void setTurn(boolean turn) {
        isTurn = turn;
    }
}