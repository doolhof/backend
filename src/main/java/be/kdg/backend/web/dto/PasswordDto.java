package be.kdg.backend.web.dto;

public class PasswordDto {
    private int userNumber;
    private String oldPassword;
    private String newPassword;

    public PasswordDto() {
    }

    public PasswordDto(int userNumber, String oldPassword, String newPassword) {
        this.userNumber = userNumber;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
