package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.GameStatus;

import java.time.LocalDateTime;
import java.util.List;

public class GameDto {
    private int gameId;
    private LocalDateTime startDate;
    private LocalDateTime time;
    private GameStatus status;
    private GameBoardDto gameBoard;
    private List<PlayerDto> playerList;
    private int turnTime;
    private Integer originalGame;

    public GameDto(int gameId, LocalDateTime startDate, LocalDateTime time, GameStatus status, GameBoardDto gameBoard, List<PlayerDto> playerList, int turnTime, Integer originalGame) {
        this.gameId = gameId;
        this.startDate = startDate;
        this.time = time;
        this.status = status;
        this.gameBoard = gameBoard;
        this.playerList = playerList;
        this.turnTime = turnTime;
        this.originalGame = originalGame;
    }

    public GameDto() {
        this.status = GameStatus.WAITING;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public GameBoardDto getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(GameBoardDto gameBoard) {
        this.gameBoard = gameBoard;
    }

    public List<PlayerDto> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<PlayerDto> playerList) {
        this.playerList = playerList;
    }

    public int getTurnTime() {
        return turnTime;
    }

    public void setTurnTime(int turnTime) {
        this.turnTime = turnTime;
    }

    public Integer getOriginalGame() {
        return originalGame;
    }

    public void setOriginalGame(Integer originalGame) {
        this.originalGame = originalGame;
    }
}
