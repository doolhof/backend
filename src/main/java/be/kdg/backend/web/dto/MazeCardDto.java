package be.kdg.backend.web.dto;

public class MazeCardDto  {
    private int mazeCardId;
    private int orientation;
    private boolean northWall;
    private boolean eastWall;
    private boolean southWall;
    private boolean westWall;
    private int imageType;
    private TreasureDto treasure;

    public MazeCardDto(int mazeCardId, int orientation, boolean northWall, boolean eastWall, boolean southWall, boolean westWall, int imageType) {
        this.mazeCardId = mazeCardId;
        this.orientation = orientation;
        this.northWall = northWall;
        this.eastWall = eastWall;
        this.southWall = southWall;
        this.westWall = westWall;
        this.imageType = imageType;
        this.treasure = null;
    }

    public MazeCardDto(int mazeCardId, int orientation, boolean northWall, boolean eastWall, boolean southWall, boolean westWall, int imageType, TreasureDto treasureDto) {
        this.mazeCardId = mazeCardId;
        this.orientation = orientation;
        this.northWall = northWall;
        this.eastWall = eastWall;
        this.southWall = southWall;
        this.westWall = westWall;
        this.imageType = imageType;
        this.treasure = treasureDto;
    }

    public MazeCardDto() {
    }

    public int getMazeCardId() {
        return mazeCardId;
    }

    public void setMazeCardId(int mazeCardId) {
        this.mazeCardId = mazeCardId;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public boolean isNorthWall() {
        return northWall;
    }

    public void setNorthWall(boolean northWall) {
        this.northWall = northWall;
    }

    public boolean isEastWall() {
        return eastWall;
    }

    public void setEastWall(boolean eastWall) {
        this.eastWall = eastWall;
    }

    public boolean isSouthWall() {
        return southWall;
    }

    public void setSouthWall(boolean southWall) {
        this.southWall = southWall;
    }

    public boolean isWestWall() {
        return westWall;
    }

    public void setWestWall(boolean westWall) {
        this.westWall = westWall;
    }

    public int getImageType() {
        return imageType;
    }

    public void setImageType(int imageType) {
        this.imageType = imageType;
    }

    public TreasureDto getTreasure() {
        return treasure;
    }

    public void setTreasure(TreasureDto treasure) {
        this.treasure = treasure;
    }
}
