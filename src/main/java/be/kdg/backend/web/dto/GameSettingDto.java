package be.kdg.backend.web.dto;

public class GameSettingDto {
    private int gameSettingId;
    private int amountOfUsers;
    private int secondsPerTurn;

    public GameSettingDto(int gameSettingId, int amountUsers, int secondsPerTurn) {
        this.gameSettingId = gameSettingId;
        this.amountOfUsers = amountUsers;
        this.secondsPerTurn = secondsPerTurn;
    }

    public GameSettingDto() {
    }

    public int getGameSettingId() {
        return gameSettingId;
    }

    public void setGameSettingId(int gameSettingId) {
        this.gameSettingId = gameSettingId;
    }

    public int getAmountOfUsers() {
        return amountOfUsers;
    }

    public void setAmountOfUsers(int amountOfUsers) {
        this.amountOfUsers = amountOfUsers;
    }

    public int getSecondsPerTurn() {
        return secondsPerTurn;
    }

    public void setSecondsPerTurn(int secondsPerTurn) {
        this.secondsPerTurn = secondsPerTurn;
    }


}
