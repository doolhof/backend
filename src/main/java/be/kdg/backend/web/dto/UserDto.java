package be.kdg.backend.web.dto;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.domain.user.UserSetting;
import be.kdg.backend.domain.user.UserStatistic;
import com.fasterxml.jackson.annotation.JsonBackReference;



import java.io.Serializable;
import java.util.List;
//fklsqjflksdqjlksmdfjslkfjd
public class UserDto implements Serializable {
    private Integer userId;

    private String username;

    private List<User> friends;

    private String encryptedPassword;

    private String oldPassword;

    private String email;

    @JsonBackReference
    private List<LobbyDto> lobbies;


    private UserStatistic userStatistic;

    private UserSetting userSetting;

    public UserDto(Integer userId, String username, String encryptedPassword, String oldPassword, String email, List<LobbyDto> lobbies, UserStatistic userStatistic, UserSetting userSetting) {
        this.userId = userId;
        this.username = username;
        this.encryptedPassword = encryptedPassword;
        this.oldPassword = oldPassword;
        this.email = email;
        this.lobbies = lobbies;
        this.userStatistic = userStatistic;
        this.userSetting = userSetting;
    }

    public UserDto() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    @JsonBackReference
    public List<LobbyDto> getLobbies() {
        return lobbies;
    }

    @JsonBackReference
    public void setLobbies(List<LobbyDto> lobbies) {
        this.lobbies = lobbies;
    }

    public UserStatistic getUserStatistic() {
        return userStatistic;
    }

    public void setUserStatistic(UserStatistic userStatistic) {
        this.userStatistic = userStatistic;
    }

    public UserSetting getUserSetting() {
        return userSetting;
    }

    public void setUserSetting(UserSetting userSetting) {
        this.userSetting = userSetting;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
