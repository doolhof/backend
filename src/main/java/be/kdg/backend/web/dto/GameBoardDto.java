package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.Tile;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

public class GameBoardDto {
    private int gameBoardId;
    private List<TileDto> tiles;

    public GameBoardDto(int gameBoardId, List<TileDto> tiles) {
        this.gameBoardId = gameBoardId;
        this.tiles = tiles;
    }

    public GameBoardDto() {
    }

    public int getGameBoardId() {
        return gameBoardId;
    }

    public void setGameBoardId(int gameBoardId) {
        this.gameBoardId = gameBoardId;
    }

    public List<TileDto> getTiles() {
        return tiles;
    }

    public void setTiles(List<TileDto> tiles) {
        this.tiles = tiles;
    }
}
