package be.kdg.backend.web.dto;

import be.kdg.backend.domain.user.NotificationType;
import be.kdg.backend.domain.user.User;

public class NotificationDto {
    private int notificationId;

    private String notifDesc;

    private UserDto user;

    private NotificationType notificationType;

    private String webUrl;
    private String mobileUrl;
    private boolean closed;

    public NotificationDto(int notificationId, String notifDesc, UserDto user, NotificationType notificationType, String webUrl, String mobileUrl, boolean closed) {
        this.notificationId = notificationId;
        this.notifDesc = notifDesc;
        this.user = user;
        this.notificationType = notificationType;
        this.webUrl = webUrl;
        this.mobileUrl = mobileUrl;
        this.closed = closed;
    }

    public NotificationDto() {
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotifDesc() {
        return notifDesc;
    }

    public void setNotifDesc(String notifDesc) {
        this.notifDesc = notifDesc;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(String mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public boolean isClosed() {
        return closed;
    }


    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
