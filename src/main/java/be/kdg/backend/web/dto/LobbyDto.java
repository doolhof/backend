package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.Game;
import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;

public class LobbyDto {

    private int lobbyId;

    private List<UserDto> listUsers;

    private GameSettingDto gameSettings;

    private UserDto host;

    private GameDto game;

    public LobbyDto(List<UserDto> listUsers, GameSettingDto gameSettings, UserDto host, GameDto game) {
        this.listUsers = listUsers;
        this.gameSettings = gameSettings;
        this.host = host;
        this.game = game;
    }

    public LobbyDto() {
    }

    public int getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(int lobbyId) {
        this.lobbyId = lobbyId;
    }

    public List<UserDto> getListUsers() {
        return listUsers;
    }

    public void setListUsers(List<UserDto> listUsers) {
        this.listUsers = listUsers;
    }

    public GameSettingDto getGameSettings() {
        return gameSettings;
    }

    public void setGameSettings(GameSettingDto gameSettings) {
        this.gameSettings = gameSettings;
    }

    public UserDto getHost() {
        return host;
    }

    public void setHost(UserDto host) {
        this.host = host;
    }

    public GameDto getGame() {
        return game;
    }

    public void setGame(GameDto game) {
        this.game = game;
    }
}
