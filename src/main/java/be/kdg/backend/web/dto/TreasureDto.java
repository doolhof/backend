package be.kdg.backend.web.dto;

public class TreasureDto {
    private int treasureId;
    private String name;

    public TreasureDto(int treasureId, String name) {
        this.treasureId = treasureId;
        this.name = name;
    }

    public TreasureDto() {
    }

    public int getTreasureId() {
        return treasureId;
    }

    public void setTreasureId(int treasureId) {
        this.treasureId = treasureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
