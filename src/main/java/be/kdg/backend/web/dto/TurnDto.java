package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.TurnPhase;
import be.kdg.backend.web.dto.PlayerDto;

import java.time.LocalDateTime;
import java.util.List;

public class TurnDto {
    private int turnId;

    private int turnDuration;

    private TurnPhase turnPhase;

    private LocalDateTime turnDate;

    private int posMazeCardMove;

    private List<Integer> playerMoves;

    private int player;

    private int orientation;

    private String playerName;

    public TurnDto(int turnDuration, TurnPhase turnPhase, LocalDateTime turnDate, int posMazeCardMove, List<Integer> playerMoves, int player, int orientation) {
        // this.turnId = turnId;
        this.turnDuration = turnDuration;
        this.turnPhase = turnPhase;
        this.turnDate = turnDate;
        this.posMazeCardMove = posMazeCardMove;
        this.playerMoves = playerMoves;
        this.player = player;
        this.orientation = orientation;
    }

    public TurnDto(int turnId, int turnDuration, TurnPhase turnPhase, LocalDateTime turnDate, int posMazeCardMove, List<Integer> playerMoves, int player, int orientation, String playerName) {
        this.turnId = turnId;
        this.turnDuration = turnDuration;
        this.turnPhase = turnPhase;
        this.turnDate = turnDate;
        this.posMazeCardMove = posMazeCardMove;
        this.playerMoves = playerMoves;
        this.player = player;
        this.orientation = orientation;
        this.playerName = playerName;
    }

    public TurnDto() {

    }

//    public int getTurnId() {
//        return turnId;
//    }
//
//    public void setTurnId(int turnId) {
//        this.turnId = turnId;
//    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }


    public TurnPhase getTurnPhase() {
        return turnPhase;
    }

    public void setTurnPhase(TurnPhase turnPhase) {
        this.turnPhase = turnPhase;
    }

    public LocalDateTime getTurnDate() {
        return turnDate;
    }

    public void setTurnDate(LocalDateTime turnDate) {
        this.turnDate = turnDate;
    }

    public int getPosMazeCardMove() {
        return posMazeCardMove;
    }

    public void setPosMazeCardMove(int posMazeCardMove) {
        this.posMazeCardMove = posMazeCardMove;
    }

    public List<Integer> getPlayerMoves() {
        return playerMoves;
    }

    public void setPlayerMoves(List<Integer> playerMoves) {
        this.playerMoves = playerMoves;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
