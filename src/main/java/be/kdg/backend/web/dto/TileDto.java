package be.kdg.backend.web.dto;

import be.kdg.backend.domain.game.GameBoard;

public class TileDto {
    private int tileId;
    private int position;
    private boolean isMoveable;
    private MazeCardDto mazeCard;


    public TileDto(int tileId, int position, boolean isMoveable, MazeCardDto mazeCard) {
        this.tileId = tileId;
        this.position = position;
        this.isMoveable = isMoveable;
        this.mazeCard = mazeCard;

    }

    public TileDto() {
    }

    public int getTileId() {
        return tileId;
    }

    public void setTileId(int tileId) {
        this.tileId = tileId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isMoveable() {
        return isMoveable;
    }

    public void setMoveable(boolean moveable) {
        isMoveable = moveable;
    }

    public MazeCardDto getMazeCard() {
        return mazeCard;
    }

    public void setMazeCard(MazeCardDto mazeCard) {
        this.mazeCard = mazeCard;
    }

}
