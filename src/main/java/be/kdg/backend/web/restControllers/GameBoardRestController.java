package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.web.dto.GameBoardDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/gameboard")
public class GameBoardRestController {
    private final GameBoardService gameBoardService;
    private final ModelMapper modelMapper;

    public GameBoardRestController(GameBoardService gameBoardService, ModelMapper modelMapper) {
        this.gameBoardService = gameBoardService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<GameBoardDto> loadGameBoard(@PathVariable Integer id) {
        GameBoard gameBoard = gameBoardService.findGameBoard(id);
        return new ResponseEntity<>(modelMapper.map(gameBoard, GameBoardDto.class), HttpStatus.OK);
    }
}
