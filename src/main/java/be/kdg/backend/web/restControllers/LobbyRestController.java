package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.LobbyNotFoundException;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.user.interfaces.GameSettingService;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.NotificationService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.dto.LobbyDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/lobby")
public class LobbyRestController {
    private LobbyService lobbyService;
    private GameSettingService gameSettingService;
    private UserService userService;
    private GameService gameService;
    private ModelMapper modelMapper;
    private final  NotificationService notificationService;

    public LobbyRestController(LobbyService lobbyService, ModelMapper modelMapper, GameSettingService gameSettingService, UserService userSerivce, NotificationService notificationService, GameService gameService, NotificationService notificationService1) {
        this.lobbyService = lobbyService;
        this.modelMapper = modelMapper;
        this.gameSettingService = gameSettingService;
        this.userService = userSerivce;
        this.gameService = gameService;
        this.notificationService = notificationService;

    }

    @PostMapping("/createLobby/{userId}/{amountOfUsers}/{secondsPerTurn}")
    @Transactional
    public ResponseEntity<LobbyDto> createLobby(@PathVariable Integer userId,
                                                @PathVariable Integer amountOfUsers,
                                                @PathVariable Integer secondsPerTurn) throws UserNotFoundException {
        GameSetting gameSetting = gameSettingService.createGameSetting(amountOfUsers, secondsPerTurn);
        User user = userService.findUserById(userId);
        Lobby lobby = lobbyService.createLobby(List.of(user), gameSetting);
        lobbyService.setLobbyHost(lobby, user);
        userService.addLobby(user, lobby);
        lobbyService.setGame(lobby, gameService.createGame());
        lobbyService.saveLobby(lobby);
        return new ResponseEntity<>(modelMapper.map(lobby, LobbyDto.class), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LobbyDto> getLobbyById(@PathVariable Integer id) throws LobbyNotFoundException {
        Lobby lobby = lobbyService.findLobbyById(id);
        return new ResponseEntity<>(modelMapper.map(lobby,
                LobbyDto.class), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/invite/{lobbyId}")
    public ResponseEntity sendInvitation(@PathVariable Integer lobbyId,
                                         @RequestParam("userIds") List<String> users) throws InterruptedException {
        List<User> userList = new ArrayList<>();
        users.forEach(user -> {
            userList.add(userService.findUserById(Integer.parseInt(user)));
        });
        Lobby lobby = lobbyService.findLobbyById(lobbyId);
        notificationService.createForLobbyInvite(userList, lobby);
        lobbyService.sendInviteByEmail(userList, lobbyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("loadLobbies/{username}")
    public ResponseEntity<List<LobbyDto>> getAllLobbiesByUser(@PathVariable String username) {
        System.out.println("in getAllLobbies");
        User user = userService.findUserByUsername(username);
        List<Lobby> lobbies = lobbyService.findLobbiesPerUser(user.getUserId());
        List<LobbyDto> lobbyDtos = new LinkedList<>();
        for (Lobby lobby : lobbies) {
            lobbyDtos.add(modelMapper.map(lobby, LobbyDto.class));
        }
        return new ResponseEntity<>(lobbyDtos, HttpStatus.OK);
    }
}
