package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.Turn;
import be.kdg.backend.services.game.interfaces.TurnService;
import be.kdg.backend.web.dto.TurnDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/turn")
public class TurnRestController {

    private final TurnService turnService;
    private final ModelMapper modelMapper;

    public TurnRestController(TurnService turnService, ModelMapper modelMapper) {
        this.turnService = turnService;
        this.modelMapper = modelMapper;
    }


    @PostMapping("/endTurn/{turnDuration}/{turnPhase}/{posMazeCardMove}/{playerMoves}/{playerId}/{orientation}")
    public ResponseEntity<TurnDto> endTurn(@PathVariable int turnDuration,
                                           @PathVariable String turnPhase,
                                           @PathVariable int posMazeCardMove,
                                           @PathVariable Integer[] playerMoves,
                                           @PathVariable int playerId,
                                           @PathVariable int orientation,
                                           @PathVariable String playerName) {
        Turn turn = turnService.endTurn(turnDuration, turnPhase, posMazeCardMove, Arrays.asList(playerMoves), playerId, orientation, playerName);
        return new ResponseEntity<>(modelMapper.map(turn, TurnDto.class), HttpStatus.OK);
    }
    @GetMapping("/getTurns/{gameId}")
    public ResponseEntity<List<TurnDto>> getTurns(@PathVariable int gameId){
        List<Turn> turns = turnService.getAllTurnsByGameId(gameId);
        List<TurnDto> turnDtos = turns.stream()
                .map(element -> modelMapper.map(element, TurnDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(turnDtos, HttpStatus.OK);
    }

}
