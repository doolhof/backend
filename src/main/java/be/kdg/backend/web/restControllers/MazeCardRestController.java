package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.services.game.interfaces.MazeCardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.MazeCardDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/mazecard")
public class MazeCardRestController {
    private final MazeCardService mazeCardService;


    public MazeCardRestController(MazeCardService mazeCardService) {
        this.mazeCardService = mazeCardService;
    }
    //TODO Overbodig?
    @CrossOrigin(origins = "*")
    @PostMapping("/updateRemaining")
    public void updateRemainingMazeCard(@RequestBody MazeCardDto mazeCardDto) {
        MazeCard mazeCard = mazeCardService.findMazeCardById(mazeCardDto.getMazeCardId());
        mazeCard = mazeCardService.changeOrientation(mazeCard, mazeCardDto.getOrientation());
        mazeCardService.saveMazeCard(mazeCard);
    }
}
