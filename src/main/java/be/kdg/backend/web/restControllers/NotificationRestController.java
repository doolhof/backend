package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.services.user.interfaces.NotificationService;
import be.kdg.backend.web.dto.NotificationDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
//djkflsdqjflksdjsjlkf
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/notification")
public class NotificationRestController {

    private final NotificationService notificationService;
    private final ModelMapper modelMapper;


    public NotificationRestController(NotificationService notificationService, ModelMapper modelMapper) {
        this.notificationService = notificationService;
        this.modelMapper = modelMapper;
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public ResponseEntity<List<NotificationDto>> getAllNotifications(@PathVariable int id) {
        List<NotificationDto> dtos = notificationService.findAllByUserId(id).stream()
                .map(element -> modelMapper.map(element, NotificationDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @CrossOrigin(origins = "*")
    @PostMapping("/close/{id}")
    public ResponseEntity<?> putNotificationToClose(@PathVariable int id){
        notificationService.setToClosed(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
