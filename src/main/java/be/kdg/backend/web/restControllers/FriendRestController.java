package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.FriendNotFoundException;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.services.user.interfaces.FriendService;
import be.kdg.backend.web.dto.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/friends")
public class FriendRestController {
    private FriendService friendService;
    private ModelMapper modelMapper;

    public FriendRestController(FriendService friendService, ModelMapper modelMapper) {
        this.friendService = friendService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<UserDto>> getFriends(@PathVariable int userId) throws UserNotFoundException {
        List<User> friends = friendService.getUserFriends(userId);
        List<UserDto> userDtos = friends.stream()
                .map(element -> modelMapper.map(element, UserDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<List<UserDto>>(userDtos, HttpStatus.OK);
    }

    @GetMapping("/{userId}/{friendId}")
    public ResponseEntity<UserDto> getFriend(@PathVariable int userId, @PathVariable int friendId) throws UserNotFoundException, FriendNotFoundException {
        User friend = friendService.getFriend(userId, friendId);
        return new ResponseEntity<UserDto>(modelMapper.map(friend, UserDto.class), HttpStatus.OK);
    }

    @PutMapping("/add/{userId}/{friendId}")
    public ResponseEntity<UserDto> addFriend(@PathVariable int userId, @PathVariable int friendId) throws UserNotFoundException {
        User user = friendService.addFriend(userId, friendId);
        return new ResponseEntity<UserDto>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }

    @PutMapping("/remove/{userId}/{friendId}")
    public ResponseEntity<UserDto> removeFriend(@PathVariable int userId, @PathVariable int friendId) throws UserNotFoundException {
        User user = friendService.removeFriend(userId, friendId);
        return new ResponseEntity<UserDto>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }
}
