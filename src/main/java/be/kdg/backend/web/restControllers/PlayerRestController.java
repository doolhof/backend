package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.exceptions.InvalidMoveException;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.PlayerDto;
import be.kdg.backend.web.dto.TileDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/player")
public class PlayerRestController {
    private final PlayerService playerService;
    private final ModelMapper modelMapper;
    private final TileService tileService;
    private final GameService gameService;

    public PlayerRestController(PlayerService playerService, ModelMapper modelMapper, TileService tileService, GameService gameService) {
        this.playerService = playerService;
        this.modelMapper = modelMapper;
        this.tileService = tileService;
        this.gameService = gameService;
    }

    @PostMapping("/move")
    public ResponseEntity<?> move(@RequestParam Integer playerId, @RequestParam Integer destination) throws InvalidMoveException {
        Player player = playerService.getPlayerByUserId(playerId);
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), destination);
        LinkedList<Tile> path = playerService.move(player, destinationTile);

        if (path.isEmpty()) {
            throw new InvalidMoveException("Invalid move");
        } else {
            List<TileDto> pathDto = path.stream()
                    .map(element -> modelMapper.map(element, TileDto.class))
                    .collect(Collectors.toList());
            return new ResponseEntity<>(pathDto, HttpStatus.OK);
        }
    }

    @PostMapping("/treasureSearch")
    public ResponseEntity<PlayerDto> treasureSearch(@RequestParam Integer playerId) {
        Player player = playerService.getPlayerByUserId(playerId);
        Tile destinationTile = tileService.findTileByPosition(player.getGame().getGameBoard(), player.getCurrentPosition());
        System.out.println(player.toString());
        return new ResponseEntity<>(modelMapper.map(playerService.updatePlayerTreasure(destinationTile, player), PlayerDto.class), HttpStatus.OK);
    }

    @PostMapping("/checkWinGame")
    public ResponseEntity<Boolean> checkWinGame(@RequestParam Integer playerId) {
        Player player = playerService.getPlayerByUserId(playerId);
        boolean won = playerService.winGame(player);
        return new ResponseEntity<>(won, HttpStatus.OK);
    }



    @PostMapping("/movePlayerTile")
    public ResponseEntity<List<PlayerDto>> movePlayerTile(@RequestParam Integer playerId, @RequestParam Integer destination) {
        Player player = playerService.getPlayerByUserId(playerId);
        List<PlayerDto> playerDtos = playerService.savePushedPosition(player, destination).stream()
                .map(element -> modelMapper.map(element, PlayerDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(playerDtos, HttpStatus.OK);
    }

    @GetMapping("/game/{id}")
    public ResponseEntity<List<PlayerDto>> getAllPlayersByGameID(@PathVariable int id) {
        Game game = gameService.getGameById(id);

        List<PlayerDto> playerDtos = game.getPlayerList().stream()
                .map(element -> modelMapper.map(element, PlayerDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(playerDtos, HttpStatus.OK);
    }
}
