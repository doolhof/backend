package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.user.RegistrationToken;
import be.kdg.backend.domain.user.Token;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.exceptions.UserServiceException;
import be.kdg.backend.services.game.interfaces.RegistrationTokenService;
import be.kdg.backend.services.user.interfaces.EmailService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.dto.PasswordDto;
import be.kdg.backend.web.dto.TokenDto;
import be.kdg.backend.web.dto.UserDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService userService;
    private ModelMapper modelMapper;
    private EmailService emailService;
    private RegistrationTokenService registrationTokenService;

    public UserRestController(UserService userService, ModelMapper modelMapper, EmailService emailService, RegistrationTokenService registrationTokenService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.emailService = emailService;
        this.registrationTokenService = registrationTokenService;
    }

    @PostMapping("/signin")
    @ApiOperation(value = "${UserController.signin}")
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 422, message = "Invalid username/password supplied")})
    public ResponseEntity<TokenDto> login(//
                                          @ApiParam("Username") @RequestParam String username, //
                                          @ApiParam("Password") @RequestParam String password) {
        if (password.startsWith("uuidFb")) {
            Token token = new Token(userService.signInFb(username, password));
            return new ResponseEntity<>(modelMapper.map(token, TokenDto.class), HttpStatus.OK);
        }
        else {
            Token token = new Token(userService.signIn(username, password));
            return new ResponseEntity<>(modelMapper.map(token, TokenDto.class), HttpStatus.OK);
        }

    }

    @GetMapping("/id/{userId}")
    public ResponseEntity<UserDto> findUserById(@PathVariable int userId) throws UserNotFoundException {
        User user = userService.findUserById(userId);

        return new ResponseEntity<>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }

    @GetMapping("/community/{userId}/{min}/{max}")
    public ResponseEntity<List<UserDto>> getCommunity(@PathVariable int userId,
                                                      @PathVariable int min,
                                                      @PathVariable int max) {
        List<User> users = userService.getCommunity(userId, min, max);
        List<UserDto> userDtos = users.stream()
                .map(element -> modelMapper.map(element, UserDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<UserDto> findUserByEmail(@PathVariable String email) throws UserNotFoundException {
        User user = userService.findUserByEmail(email);
        return new ResponseEntity<>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }

    @GetMapping("/username/{username}")
    public ResponseEntity<UserDto> findByUsername(@PathVariable String username) throws UserNotFoundException {
        User user = userService.findUserByUsername(username);
        return new ResponseEntity<>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }

    @GetMapping("/usernames/{search}/{userId}")
    public ResponseEntity<List<UserDto>> searchUser(@PathVariable String search, @PathVariable int userId) {
        List<User> users = userService.searchUsers(search, userId);
        List<UserDto> userDtos = users.stream()
                .map(element -> modelMapper.map(element, UserDto.class))
                .collect(Collectors.toList());

        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }


    @PostMapping("/sign-up")
    public ResponseEntity<UserDto> signUp(@RequestBody UserDto userDTO) throws UserServiceException {
        System.out.println("in signup");
        User user = modelMapper.map(userDTO, User.class);
        user.setConfirmed(false);

        if (userService.checkUserNotExist(user)) {
            User userSaved = userService.saveUser(user);
            List<User> users = new ArrayList<>();
            users.add(userSaved);
            emailService.sendSimpleMessage("Registration", "You are registered", users, 0);
            return new ResponseEntity<>(modelMapper.map(userSaved, UserDto.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/sign-up-fb")
    public ResponseEntity<UserDto> signUpFb(@RequestBody String fbString) throws JSONException, UserServiceException {
        JSONObject jsonObject = new JSONObject(fbString);
        String email = jsonObject.getJSONObject("facebookInfo").getString("email");
        String name =  jsonObject.getJSONObject("facebookInfo").getString("name");
        User user = new User();
        user.setConfirmed(true);
        user.setUsername(name);
        user.setEmail(email);
        user.setEncryptedPassword(generatePassword());
        if (userService.checkUserNotExist(user)) {
            User userSaved = userService.saveUser(user);
            return new ResponseEntity<>(modelMapper.map(userSaved, UserDto.class), HttpStatus.OK);
        } else {
            User userFound = userService.findUserByUsername(name);
            return new ResponseEntity<>(modelMapper.map(userFound, UserDto.class), HttpStatus.OK);
        }
    }

    @PostMapping("/confirm")
    public ResponseEntity<?> confirmRegistration(@RequestParam String token) {
        System.out.println("confirm registration " + token);
        registrationTokenService.confirmToken(token);
        return new ResponseEntity<RegistrationToken>(HttpStatus.OK);

    }

    @DeleteMapping("/deleteAccount/{userId}")
    public ResponseEntity<?> deleteUserById(@PathVariable int userId) throws UserNotFoundException {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PutMapping("/update")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto) throws UserNotFoundException {
        User user = modelMapper.map(userDto, User.class);
        userService.updateUser(user);
        return new ResponseEntity<>(modelMapper.map(user, UserDto.class), HttpStatus.OK);
    }

    @PutMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestBody PasswordDto passwordDto) throws UsernameNotFoundException {
        userService.updatePassword(passwordDto.getUserNumber(),passwordDto.getOldPassword(),passwordDto.getNewPassword());
        return new ResponseEntity<>(HttpStatus.OK);

    }

    private static String generatePassword() {
        String uuid = "uuidFb"+UUID.randomUUID().toString();
        return uuid;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/updateFcmToken")
    public ResponseEntity<UserDto> updateFcmToken(@RequestParam String token, @RequestParam int id){
        User u = userService.findUserById(id);
        u.setFcmToken(token);
        userService.updateUser(u);
        return new ResponseEntity<>(modelMapper.map(u,UserDto.class),HttpStatus.OK);
    }
}
