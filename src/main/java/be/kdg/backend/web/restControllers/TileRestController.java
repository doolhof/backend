package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.web.dto.GameBoardDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/tile")
public class TileRestController {
    private final TileService tileService;
    private final GameBoardService gameBoardService;
    private final ModelMapper modelMapper;

    public TileRestController(TileService tileService, GameBoardService gameBoardService, ModelMapper modelMapper) {
        this.tileService = tileService;
        this.gameBoardService = gameBoardService;
        this.modelMapper = modelMapper;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/moveTile")
    public ResponseEntity<GameBoardDto> moveMazeCard(@RequestBody int position, @RequestParam int gameboardId) {
        System.out.println(gameboardId);
        tileService.pushMazeCard(position, gameboardId);
        GameBoard gameBoard = gameBoardService.findGameBoard(gameboardId);
        return new ResponseEntity<>(modelMapper.map(gameBoard, GameBoardDto.class), HttpStatus.OK);
    }
}
