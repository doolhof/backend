package be.kdg.backend.web.restControllers;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.CreatePlayerException;
import be.kdg.backend.exceptions.GameNotFoundException;
import be.kdg.backend.exceptions.LobbyNotFoundException;
import be.kdg.backend.exceptions.TooManyPlayersException;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.web.dto.GameDto;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/game")
public class GameRestController {
    private final GameService gameService;
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final LobbyService lobbyService;

    public GameRestController(GameService gameService, ModelMapper modelMapper, UserService userService, LobbyService lobbyService) {
        this.gameService = gameService;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.lobbyService = lobbyService;
    }

    @GetMapping("/startGame/{lobbyId}")
    public ResponseEntity<GameDto> startGame(@PathVariable int lobbyId) throws TooManyPlayersException, CreatePlayerException, LobbyNotFoundException {

        Lobby lobby = lobbyService.findLobbyById(lobbyId);
        Game game = gameService.startGame(lobby.getGame(), lobby.getListUsers(), lobby.getGameSettings().getSecondsPerTurn());
        lobbyService.setGame(lobby, game);
        lobbyService.saveLobby(lobby);
        return new ResponseEntity<>(modelMapper.map(game, GameDto.class), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GameDto> getGameById(@PathVariable Integer id) throws GameNotFoundException {
        Game game = gameService.getGameById(id);
        return new ResponseEntity<>(modelMapper.map(game, GameDto.class), HttpStatus.OK);
    }

    @GetMapping("loadAllGames/{username}")
    public ResponseEntity<List<GameDto>> getAllGamesByUser(@PathVariable String username) {
        //TODO: enkel de games van een user, momenteel nog geen users gelinkt aan game dus nog niet mogelijk
        List<Game> games = gameService.findAllGames();
        List<GameDto> gamesDtos = new LinkedList<>();
        for (Game game : games) {
            gamesDtos.add(modelMapper.map(game, GameDto.class));
        }
        return new ResponseEntity<>(gamesDtos, HttpStatus.OK);
    }

    @GetMapping("/replayGame/{gameId}")
    public ResponseEntity<GameDto> getGameForReplay(@PathVariable Integer gameId) {
        Game replayGame = gameService.getGameForReplay(gameId);
        return new ResponseEntity<>(modelMapper.map(replayGame, GameDto.class), HttpStatus.OK);
    }

    @PostMapping("/stopReplayGame/{gameId}")
    public ResponseEntity<GameDto> stopReplayGame(@PathVariable Integer gameId) {
        gameService.deleteGame(gameId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
