package be.kdg.backend.domain.game;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the model class for a Turn of a Player.
 */
@Entity
@Table(name = "TURN")
public class Turn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int turnId;

    @Column
    private int turnDuration;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TurnPhase turnPhase;

    @Column
    private LocalDateTime turnDate;

    @Column
    private int posMazeCardMove;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Integer> playerMoves;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(name = "PLAYER_ID")
    @JsonIgnore
    private Player player;

    @Column
    private String playerName;

    @Column
    private int orientation;


    public Turn(int turnDuration, TurnPhase turnPhase, LocalDateTime turnDate, int posMazeCardMove, List<Integer> playerMoves, Player player, int orientation) {
        this.turnDuration = turnDuration;
        this.turnPhase = turnPhase;
        this.turnDate = turnDate;
        this.posMazeCardMove = posMazeCardMove;
        this.playerMoves = playerMoves;
        this.player = player;
        this.orientation = orientation;
    }

    public Turn(int turnDuration, TurnPhase turnPhase, LocalDateTime turnDate, int posMazeCardMove, List<Integer> playerMoves, Player player, String playerName, int orientation) {
        this.turnDuration = turnDuration;
        this.turnPhase = turnPhase;
        this.turnDate = turnDate;
        this.posMazeCardMove = posMazeCardMove;
        this.playerMoves = playerMoves;
        this.player = player;
        this.playerName = playerName;
        this.orientation = orientation;
    }

    public Turn() {
    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }

    public int getTurnId() {
        return turnId;
    }

    public void setTurnId(int turnId) {
        this.turnId = turnId;
    }

    public int getPosMazeCardMove() {
        return posMazeCardMove;
    }

    public void setPosMazeCardMove(int posMazeCardMove) {
        this.posMazeCardMove = posMazeCardMove;
    }

    public List<Integer> getPlayerMoves() {
        return playerMoves;
    }

    public void setPlayerMoves(List<Integer> playerMoves) {
        this.playerMoves = playerMoves;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public LocalDateTime getTurnDate() {
        return turnDate;
    }

    public void setTurnDate(LocalDateTime turnDate) {
        this.turnDate = turnDate;
    }

    public TurnPhase getTurnPhase() {
        return turnPhase;
    }

    public void setTurnPhase(TurnPhase turnPhase) {
        this.turnPhase = turnPhase;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }
}
