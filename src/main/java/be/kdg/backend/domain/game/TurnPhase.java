package be.kdg.backend.domain.game;

public enum TurnPhase {
    CARDMOVE, PLAYERMOVE
}
