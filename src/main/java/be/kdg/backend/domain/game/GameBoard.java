package be.kdg.backend.domain.game;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

/**
 * This is the model class for the gameboard.
 */
@Entity
@Table(name="GAME_BOARD")
public class GameBoard {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int gameBoardId;

    @OneToMany(targetEntity = Tile.class, mappedBy = "gameBoard", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Tile> tiles;

    public GameBoard(List<Tile> tile) {
        this.tiles = tile;
    }

    public GameBoard() {
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(List<Tile> tiles) {
        this.tiles = tiles;
    }

    public int getGameBoardId() {
        return gameBoardId;
    }

    public void setGameBoardId(int gameBoardId) {
        this.gameBoardId = gameBoardId;
    }
}
