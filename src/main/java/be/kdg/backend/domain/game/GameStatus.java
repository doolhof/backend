package be.kdg.backend.domain.game;

public enum GameStatus {
    WAITING, STARTED, FINISHED
}
