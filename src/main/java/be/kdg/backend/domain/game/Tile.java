package be.kdg.backend.domain.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

/**
 * This is the model class that presents a specific field on the gameboard.
 */
@Entity
@Table(name = "TILE")
public class Tile implements Comparable<Tile> {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int tileId;

    @Column
    private int position;

    @Column
    private boolean isMoveable;

    @OneToOne(targetEntity = MazeCard.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "MAZE_CARD_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MazeCard mazeCard;

    @ManyToOne(targetEntity = GameBoard.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "GAME_BOARD_ID")
    private GameBoard gameBoard;

    public Tile(int position, boolean isMoveable, MazeCard mazeCard, GameBoard gameBoard) {
        this.position = position;
        this.isMoveable = isMoveable;
        this.mazeCard = mazeCard;
        this.gameBoard = gameBoard;
    }

    public Tile() {
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isMoveable() {
        return isMoveable;
    }

    public void setMoveable(boolean moveable) {
        isMoveable = moveable;
    }

    public MazeCard getMazeCard() {
        return mazeCard;
    }

    public void setMazeCard(MazeCard mazeCard) {
        this.mazeCard = mazeCard;
    }


    @Override
    public int compareTo(Tile o) {
        int comparePosition = (o).getPosition();
        return this.getPosition() - comparePosition;
    }

    public int getTileId() {
        return tileId;
    }

    public void setTileId(int tileId) {
        this.tileId = tileId;
    }

    @JsonIgnore
    public GameBoard getGameBoard() {
        return gameBoard;
    }

    @JsonIgnore
    public void setGameBoard(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }
}
