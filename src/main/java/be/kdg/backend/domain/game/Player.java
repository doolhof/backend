package be.kdg.backend.domain.game;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

/**
 * This is the model class for a Player in a game.
 */
@Entity
@Table(name = "PLAYER")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int playerId;

    @Column(name = "color", nullable = false)
    @Enumerated(EnumType.STRING)
    private Color color;

    @Column
    private String name;

    //TODO: mss kunnen we dit ook in 1 map doen met een waarde found true/false?
    @OneToMany(targetEntity = Treasure.class, mappedBy = "playerFind", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Treasure> toFind;
    @OneToMany(targetEntity = Treasure.class, mappedBy = "playerFound", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Treasure> found;

    @OneToMany(targetEntity = Turn.class, mappedBy = "player", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Turn> turns;

    @ManyToOne(targetEntity = Game.class)
    @JoinColumn(name = "GAME_ID")
    private Game game;

    @Column
    private int currentPosition;
    @Column
    private int startPosition;
    @Column
    private boolean isTurn;

    public Player(Color color, String name, int startPosition, int currentPosition, List<Treasure> toFind, List<Treasure> found, List<Turn> turns, Game game, boolean isTurn) {
        this.color = color;
        this.name = name;
        this.startPosition = startPosition;
        this.currentPosition = currentPosition;
        this.toFind = toFind;
        this.found = found;
        this.turns = turns;
        this.game = game;
        this.isTurn = isTurn;
    }

    public Player() {
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Treasure> getToFind() {
        return toFind;
    }

    public void setToFind(List<Treasure> toFind) {
        this.toFind = toFind;
    }

    public List<Treasure> getFound() {
        return found;
    }

    public void setFound(List<Treasure> found) {
        this.found = found;
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public boolean isTurn() {
        return isTurn;
    }

    public void setTurn(boolean turn) {
        isTurn = turn;
    }
}
