package be.kdg.backend.domain.game;

public enum TreasureEnum {
    CROWN, MAP, BOW, CHEST, SHIELD, RAT, HELM, ROPE, CROW, SPIDER, KEYS, RING, BOOK, DRAGON, LAMP, SWORD, POUCH, LIZARD, CITY, DWARF
}
