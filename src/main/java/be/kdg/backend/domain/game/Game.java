package be.kdg.backend.domain.game;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * This is a model class for a game.
 */
@Entity
@Table(name = "GAME")
public class Game {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int gameId;

    @Column
    private LocalDateTime startDate;

    @Column
    private LocalDateTime time;

    @Column
    private GameStatus status;

    @OneToOne(targetEntity = GameBoard.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "GAME_BOARD_ID")
    private GameBoard gameBoard;

    @OneToOne(targetEntity = GameBoard.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "START_GAME_BOARD_ID")
    private GameBoard startGameBoard;

    @OneToMany(targetEntity = Player.class, mappedBy = "game", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Player> playerList;

    @Column
    private Integer turnTime;

    @Column
    private Integer originalGame;


    public Game(LocalDateTime startDate, LocalDateTime time, GameStatus status, GameBoard gameBoard, List<Player> playerList, Integer turnTime) {
        this.startDate = startDate;
        this.time = time;
        this.status = status;
        this.gameBoard = gameBoard;
        this.playerList = playerList;
        this.turnTime = turnTime;
    }

    public Game(LocalDateTime startDate, LocalDateTime time, GameBoard gameBoard, GameBoard startGameBoard, List<Player> playerList, Integer turnTime) {
        this.startDate = startDate;
        this.time = time;
        this.gameBoard = gameBoard;
        this.startGameBoard = startGameBoard;
        this.playerList = playerList;
        this.turnTime = turnTime;
    }


    public Game() {
        this.status = GameStatus.WAITING;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public Integer getTurnTime() {
        return turnTime;
    }

    public void setTurnTime(Integer turnTime) {
        this.turnTime = turnTime;
    }

    public GameBoard getStartGameBoard() {
        return startGameBoard;
    }

    public void setStartGameBoard(GameBoard startGameBoard) {
        this.startGameBoard = startGameBoard;
    }

    public Integer getOriginalGame() {
        return originalGame;
    }

    public void setOriginalGame(Integer originalGame) {
        this.originalGame = originalGame;
    }
}
