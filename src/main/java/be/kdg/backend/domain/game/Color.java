package be.kdg.backend.domain.game;

/**
 * This enum contains the possible colors that a player can be.
 * A player can only be one color at the same time.
 */


public enum Color {
    DODGERBLUE,GREEN,RED,YELLOW
}
