package be.kdg.backend.domain.game;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.*;

/**
 * This is the model class for a card that can be put on a Tile of the Gameboard.
 */

@Entity
@Table(name = "MAZECARD")
public class MazeCard {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int mazeCardId;

    @Column
    private int orientation;

    @Column
    private boolean northWall;

    @Column
    private boolean eastWall;

    @Column
    private boolean southWall;

    @Column
    private boolean westWall;

    //1 = corner,   2 = straight,   3 = T
    @Column
    private int imageType;

    @OneToOne
    private Treasure treasure;

    public MazeCard(int orientation, boolean northWall, boolean eastWall, boolean southWall, boolean westWall, int imageType) {
        this.orientation = orientation;
        this.northWall = northWall;
        this.eastWall = eastWall;
        this.southWall = southWall;
        this.westWall = westWall;
        this.imageType = imageType;
    }

    public MazeCard(int orientation, boolean northWall, boolean eastWall, boolean southWall, boolean westWall, int imageType, Treasure treasure) {
        this.orientation = orientation;
        this.northWall = northWall;
        this.eastWall = eastWall;
        this.southWall = southWall;
        this.westWall = westWall;
        this.imageType = imageType;
        this.treasure = treasure;
    }

    public MazeCard() {
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }


    public boolean isNorthWall() {
        return northWall;
    }

    public boolean isEastWall() {
        return eastWall;
    }

    public boolean isSouthWall() {
        return southWall;
    }

    public boolean isWestWall() {
        return westWall;
    }

    public int getMazeCardId() {
        return mazeCardId;
    }

    public void setNorthWall(boolean northWall) {
        this.northWall = northWall;
    }

    public void setEastWall(boolean eastWall) {
        this.eastWall = eastWall;
    }

    public void setSouthWall(boolean southWall) {
        this.southWall = southWall;
    }

    public void setWestWall(boolean westWall) {
        this.westWall = westWall;
    }

    public void setImageType(int imageType) {
        this.imageType = imageType;
    }

    public Treasure getTreasure() {
        return treasure;
    }

    public void setTreasure(Treasure treasure) {
        this.treasure = treasure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MazeCard mazeCard = (MazeCard) o;
        return mazeCardId == mazeCard.mazeCardId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mazeCardId, orientation, northWall, eastWall, southWall, westWall, imageType);
    }

    public void setMazeCardId(int mazeCardId) {
        this.mazeCardId = mazeCardId;
    }

    public int getImageType() {
        return imageType;
    }
}
