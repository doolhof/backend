package be.kdg.backend.domain.game;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * This is the model class for a treasure that a player needs to find.
 */
@Entity
@Table(name = "TREASURE")
public class Treasure {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int treasureId;

    @Column
    private String name;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(name = "player_Find_ID", referencedColumnName="playerId")
    private Player playerFind;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(name = "player_Found_ID", referencedColumnName="playerId")
    private Player playerFound;

    @OneToOne(targetEntity = MazeCard.class)
    @JoinColumn(name="MAZE_CARD_ID")
    private MazeCard mazeCard;


    public Treasure(String name) {
        this.name = name;
    }

    public Treasure(int treasureId, String name) {
        this.treasureId = treasureId;
        this.name = name;
    }

    public Treasure(String name, Player playerFind, Player playerFound, MazeCard mazeCard) {
        this.name = name;
        this.playerFind = playerFind;
        this.playerFound = playerFound;
        this.mazeCard = mazeCard;
    }

    public Treasure() {
    }

    public Player getPlayerFind() {
        return playerFind;
    }

    public void setPlayerFind(Player player) {
        this.playerFind = player;
    }

    public Player getPlayerFound() {
        return playerFound;
    }

    public void setPlayerFound(Player playerFound) {
        this.playerFound = playerFound;
    }

    public MazeCard getMazeCard() {
        return mazeCard;
    }

    public void setMazeCard(MazeCard mazeCard) {
        this.mazeCard = mazeCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTreasureId() {
        return treasureId;
    }

    public void setTreasureId(int treasureId) {
        this.treasureId = treasureId;
    }

    @Override
    public String toString() {
        return "Treasure: " + name;
    }
}
