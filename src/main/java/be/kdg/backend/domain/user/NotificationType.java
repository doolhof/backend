package be.kdg.backend.domain.user;

public enum NotificationType {
    LOBBYINVITE, TURN
}
