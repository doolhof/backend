package be.kdg.backend.domain.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * This is a model class that contains the statistics of a user like the amount of games played, wins, losts
 */
@Entity
public class UserStatistic {
    @Column(nullable = false)
    @Id
    @GeneratedValue
    private Integer statisticId;
    @Column
    private int gamesPlayed;
    @Column
    private int gamesWon;
    @Column
    private int gamesLost;

    public UserStatistic(int gamesPlayed, int gamesWon, int gamesLost) {
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
    }

    public UserStatistic(Integer statisticId, Integer gamesPlayed, Integer gamesWon, Integer gamesLost) {
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
        this.statisticId = statisticId;
    }


    public UserStatistic() {
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }


    public Integer getStatisticId() {
        return statisticId;
    }

    public void setStatisticId(Integer statisticId) {
        this.statisticId = statisticId;
    }
}
