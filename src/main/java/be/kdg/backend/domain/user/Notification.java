package be.kdg.backend.domain.user;

import be.kdg.backend.domain.game.MazeCard;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name = "NOTIFICATION")
public class Notification {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int notificationId;

    @Column
    private String notifDesc;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_ID")
    @JsonManagedReference("notifications")
    private User user;

    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;

    @Column
    private String webUrl;
    @Column
    private String mobileUrl;

    @Column
    private boolean closed;

    public Notification(String notifDesc, User user, NotificationType notificationType, String webUrl, String mobileUrl, boolean closed) {
        this.notifDesc = notifDesc;
        this.user = user;
        this.notificationType = notificationType;
        this.webUrl = webUrl;
        this.mobileUrl = mobileUrl;
        this.closed = closed;
    }

    public Notification() {
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }


    public String getNotifDesc() {
        return notifDesc;
    }

    public void setNotifDesc(String notifDesc) {
        this.notifDesc = notifDesc;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(String mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
