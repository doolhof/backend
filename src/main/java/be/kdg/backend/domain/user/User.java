package be.kdg.backend.domain.user;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a model class for a user.
 */

@Entity
@Table(name = "\"USER\"")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "userId"
)
public class User {
    @Id
    @GeneratedValue
    @Column(nullable = false)
    @JsonManagedReference("friends")
    private Integer userId;

    @Unique
    @Column
    private String username;

    @OneToMany
    @JoinTable(name = "FRIENDS")
    @JoinColumn(name = "USER_ID", referencedColumnName = "userId")
    @JoinColumn(name = "FRIEND_ID", referencedColumnName = "userId")
    @JsonBackReference("friends")
    private List<User> friends;

    @Column
    private String email;

    @Column
    private String encryptedPassword;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "listUsers")
    @JsonBackReference
    private List<Lobby> lobbies;

    @OneToOne(targetEntity = UserStatistic.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "statisticId", nullable = false)
    private UserStatistic userStatistic;

    @OneToOne(targetEntity = UserSetting.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "settingId", nullable = false)
    private UserSetting userSetting;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles;

    @Column
    private boolean confirmed;

    @OneToMany(targetEntity = Notification.class, mappedBy = "user", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonBackReference("notifications")
    private List<Notification> notifications;

    @Column
    private String fcmToken;

    public User(String username, String email, String encryptedPassword, List<Lobby> lobbies, UserStatistic userStatistic, UserSetting userSetting, List<Role> roles, boolean confirmed, List<Notification> notifications, String fcmToken) {
        this.username = username;
        this.email = email;
        this.encryptedPassword = encryptedPassword;
        this.lobbies = lobbies;
        this.userStatistic = userStatistic;
        this.userSetting = userSetting;
        this.confirmed = confirmed;
        this.friends = new ArrayList<>();
    }

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public List<Lobby> getLobbies() {
        return lobbies;
    }

    public void setLobbies(List<Lobby> lobbies) {
        this.lobbies = lobbies;
    }

    public UserStatistic getUserStatistic() {
        return userStatistic;
    }

    public void setUserStatistic(UserStatistic userStatistic) {
        this.userStatistic = userStatistic;
    }

    public UserSetting getUserSetting() {
        return userSetting;
    }

    public void setUserSetting(UserSetting userSetting) {
        this.userSetting = userSetting;
    }

    public Integer getUserId() {
        return userId;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public void setUserId (Integer userId){
        this.userId = userId;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
