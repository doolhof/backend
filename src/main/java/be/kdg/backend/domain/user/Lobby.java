package be.kdg.backend.domain.user;

import be.kdg.backend.domain.game.Game;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * This is a model class that contains all the listUsers for a specific game.
 */
@Entity
@Table(name = "LOBBY")
public class Lobby {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int lobbyId;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE})
    @JoinTable(name = "user_lobby", joinColumns = {@JoinColumn(name = "lobby_id")}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private List<User> listUsers;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private GameSetting gameSettings;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private User host;

    public Lobby(List<User> users, GameSetting gameSettings) {
        this.listUsers = users;
        this.gameSettings = gameSettings;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GAME_ID")
    private Game game;

    public Lobby() {
    }

    public int getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(int lobbyId) {
        this.lobbyId = lobbyId;

    }

    public List<User> getListUsers() {
        return listUsers;
    }

    public void setListUsers(List<User> listUsers) {
        this.listUsers = listUsers;
    }

    public GameSetting getGameSettings() {
        return gameSettings;
    }

    public void setGameSettings(GameSetting gameSettings) {

        this.gameSettings = gameSettings;
    }

    public User getHost() {
        return host;
    }

    public void setHost(User host) {
        this.host = host;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "Lobby{" +
                "lobbyId=" + lobbyId +
                ", listUsers=" + listUsers +
                ", gameSettings=" + gameSettings +
                '}';
    }
}