package be.kdg.backend.domain.user;

/**
 * This is a model class that presents an invite for a specific lobby.
 */
public class GameInvite {
    private int inviteId;
    private Lobby lobby;

    public GameInvite(int inviteId, Lobby lobby) {
        this.inviteId = inviteId;
        this.lobby = lobby;
    }

    public GameInvite() {
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }
}
