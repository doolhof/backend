package be.kdg.backend.domain.user;

import java.util.List;

/**
 * this is a model class for the community. All the users are in the community.
 */
public class Community {
    private List<User> users;

    public Community(List<User> users) {
        this.users = users;
    }

    public Community() {
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
