package be.kdg.backend.domain.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * This is a model class that contains al the settings from a user.
 */
@Entity
public class UserSetting {
    @Column(nullable = false)
    @Id
    @GeneratedValue
    private Integer settingId;

    @Column
    private boolean receiveInvite;
    @Column
    private boolean turnPlayed;
    @Column
    private boolean myTurn;
    @Column
    private boolean turnExpiring;
    @Column
    private boolean incomingMessage;

    public UserSetting() {

    }

    public UserSetting(boolean receiveInvite, boolean turnPlayed, boolean myTurn, boolean turnExpiring, boolean incomingMessage) {
        this.receiveInvite = receiveInvite;
        this.turnPlayed = turnPlayed;
        this.myTurn = myTurn;
        this.turnExpiring = turnExpiring;
        this.incomingMessage = incomingMessage;
    }

    public Integer getSettingId() {
        return settingId;
    }

    public boolean isReceiveInvite() {
        return receiveInvite;
    }

    public void setReceiveInvite(boolean receiveInvite) {
        this.receiveInvite = receiveInvite;
    }

    public boolean isTurnPlayed() {
        return turnPlayed;
    }

    public void setTurnPlayed(boolean turnPlayed) {
        this.turnPlayed = turnPlayed;
    }

    public boolean isMyTurn() {
        return myTurn;
    }

    public void setMyTurn(boolean myTurn) {
        this.myTurn = myTurn;
    }

    public boolean isTurnExpiring() {
        return turnExpiring;
    }

    public void setTurnExpiring(boolean turnExpiring) {
        this.turnExpiring = turnExpiring;
    }

    public boolean isIncomingMessage() {
        return incomingMessage;
    }

    public void setIncomingMessage(boolean incomingMessage) {
        this.incomingMessage = incomingMessage;
    }
}
