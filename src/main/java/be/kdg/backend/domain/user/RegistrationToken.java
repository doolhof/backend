package be.kdg.backend.domain.user;

import javax.persistence.*;

@Entity
@Table(name="REGISTRATION_TOKEN")
public class RegistrationToken {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column
    private int id;
    @Column
    private String token;
    @OneToOne(targetEntity = User.class,fetch=FetchType.EAGER)
    @JoinColumn(nullable = false,name="user_id")
    private User user;

    public RegistrationToken(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public RegistrationToken() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
