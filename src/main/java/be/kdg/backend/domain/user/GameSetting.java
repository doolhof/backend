package be.kdg.backend.domain.user;

import be.kdg.backend.domain.game.Color;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name="GAME_SETTING")
public class GameSetting {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(nullable = false)
    private int gameSettingId;
    @Column
    private int amountOfUsers;
    @Column
    private int secondsPerTurn;


    public GameSetting() {
    }

    public GameSetting(int amountUsers, int secondsPerTurn) {
        this.amountOfUsers = amountUsers;
        this.secondsPerTurn = secondsPerTurn;
    }

    public int getGameSettingId() {
        return gameSettingId;
    }

    public void setGameSettingId(int gameSettingId) {
        this.gameSettingId = gameSettingId;
    }

    public int getAmountOfUsers() {
        return amountOfUsers;
    }

    public void setAmountOfUsers(int amountOfUsers) {
        this.amountOfUsers = amountOfUsers;
    }

    public int getSecondsPerTurn() {
        return secondsPerTurn;
    }

    public void setSecondsPerTurn(int secondsPerTurn) {
        this.secondsPerTurn = secondsPerTurn;
    }

    @Override
    public String toString() {
        return "GameSetting{" +
                "gameSettingId=" + gameSettingId +
                ", amountOfUsers=" + amountOfUsers +
                ", secondsPerTurn=" + secondsPerTurn +
                '}';
    }
}





