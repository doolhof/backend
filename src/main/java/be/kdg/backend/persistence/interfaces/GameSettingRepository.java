package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.GameSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameSettingRepository extends JpaRepository<GameSetting, Integer> {
}
