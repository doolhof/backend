package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.RegistrationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistrationTokenRepository extends JpaRepository<RegistrationToken, Integer> {
}
