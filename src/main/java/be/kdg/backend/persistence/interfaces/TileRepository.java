package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Tile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is the interface for the tileRepository
 */
public interface TileRepository extends JpaRepository<Tile, Integer> {
    Tile findTileByGameBoardAndPosition(GameBoard gameBoard, int position);

}
