package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.GameBoard;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is the interface for the gameBoard repository
 */
public interface GameBoardRepository extends JpaRepository<GameBoard, Integer> {

}
