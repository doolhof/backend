package be.kdg.backend.persistence.interfaces;
import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Turn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TurnRepository extends JpaRepository<Turn, Integer> {


    @Query("FROM Turn t where t.player.game.gameId = :gameId")
    List<Turn> findTurnsByPlayerGame_GameId(@Param("gameId") Integer gameId);

    Turn findTurnByPlayer(Player player);
    List<Turn> findTurnsByPlayerGame_Game(Game game);

}
