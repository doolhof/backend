package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    @Query("FROM User u where u.userId <> :userId ORDER BY u.username ASC")
    List<User> findUsers(@Param("userId") Integer userId, Pageable max);

    List<User> findUsersByUsernameContainingIgnoreCaseAndUserIdNot (String search, int userId);
}
