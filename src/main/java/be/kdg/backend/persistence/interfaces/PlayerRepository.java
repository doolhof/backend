package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
}
