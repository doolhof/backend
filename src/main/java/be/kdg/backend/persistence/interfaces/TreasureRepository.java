package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.Treasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreasureRepository extends JpaRepository<Treasure, Integer> {
}
