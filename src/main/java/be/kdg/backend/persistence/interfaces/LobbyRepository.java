package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.Lobby;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LobbyRepository extends JpaRepository<Lobby,Integer> {
}
