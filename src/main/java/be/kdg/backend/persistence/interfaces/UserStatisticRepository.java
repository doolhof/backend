package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.UserStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserStatisticRepository extends JpaRepository<UserStatistic, Integer> {
}
