package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Integer> {
}
