package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This is the interface for the mazeCard Repository
 */
public interface MazeCardRepository extends JpaRepository<MazeCard, Integer> {
}
