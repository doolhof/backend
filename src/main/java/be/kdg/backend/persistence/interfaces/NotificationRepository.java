package be.kdg.backend.persistence.interfaces;

import be.kdg.backend.domain.user.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification,Integer> {
}
