package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.AlreadyFriendsException;
import be.kdg.backend.exceptions.FriendNotFoundException;
import be.kdg.backend.exceptions.UserNotFoundException;

import java.util.List;

public interface FriendService {

    User addFriend(int userId, int friendId) throws UserNotFoundException, AlreadyFriendsException;

    List<User> getUserFriends(int userId) throws UserNotFoundException;

    User removeFriend(int userId, int friendId) throws UserNotFoundException;

    User getFriend(int userId, int friendId) throws UserNotFoundException, FriendNotFoundException;
}
