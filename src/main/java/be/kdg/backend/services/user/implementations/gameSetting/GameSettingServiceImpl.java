package be.kdg.backend.services.user.implementations.gameSetting;

import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.persistence.interfaces.GameSettingRepository;
import be.kdg.backend.services.user.interfaces.GameSettingService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("GameSettingService")
@Transactional
public class GameSettingServiceImpl implements GameSettingService {
    private final GameSettingRepository gameSettingRepository;

    public GameSettingServiceImpl(GameSettingRepository gameSettingRepository) {
        this.gameSettingRepository = gameSettingRepository;
    }

    @Override
    public GameSetting createGameSetting(int amountOfUsers, int secondsPerTurn) {
        return save(new GameSetting(amountOfUsers, secondsPerTurn));
    }

    @Override
    public GameSetting save(GameSetting gameSetting) {
        return gameSettingRepository.save(gameSetting);
    }
}
