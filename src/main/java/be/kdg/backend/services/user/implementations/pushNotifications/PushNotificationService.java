package be.kdg.backend.services.user.implementations.pushNotifications;

import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.services.user.implementations.pushNotifications.HeaderRequestInterceptor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

@Service
public class PushNotificationService {
    @Value("${FIREBASE.SERVER.KEY}")
    private String FIREBASE_SERVER_KEY;
    @Value("${FIREBASE.API.URL}")
    private String FIREBASE_API_URL;

    @Async
    public void send(Notification nObject) {

        if (nObject.getUser().getFcmToken() != null) {

            JSONObject body = new JSONObject();
            body.put("to", nObject.getUser().getFcmToken());
            body.put("priority", "high");

            JSONObject notification = new JSONObject();
            notification.put("title", nObject.getNotificationType());
            notification.put("body", nObject.getNotifDesc());
            notification.put("click_action", nObject.getWebUrl());

            body.put("notification", notification);

            HttpEntity<String> request = new HttpEntity<>(body.toString());

            RestTemplate restTemplate = new RestTemplate();

            ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
            interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
            interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
            restTemplate.setInterceptors(interceptors);

            String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, request, String.class);

            CompletableFuture<String> pushNotification = CompletableFuture.completedFuture(firebaseResponse);
            CompletableFuture.allOf(pushNotification).join();
        }
    }


}
