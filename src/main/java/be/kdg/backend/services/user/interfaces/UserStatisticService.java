package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.game.Player;

public interface UserStatisticService {
    void UpdateStatistics(Player player);
}
