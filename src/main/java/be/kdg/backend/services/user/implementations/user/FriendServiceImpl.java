package be.kdg.backend.services.user.implementations.user;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.AlreadyFriendsException;
import be.kdg.backend.exceptions.FriendNotFoundException;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.persistence.interfaces.UserRepository;
import be.kdg.backend.services.user.interfaces.FriendService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service("FriendService")
@Transactional
public class FriendServiceImpl implements FriendService{
    private UserRepository userRepository;

    public FriendServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User addFriend(int userId, int friendId) throws UserNotFoundException, AlreadyFriendsException {
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isPresent()) {

            Optional<User> friendOptional = userRepository.findById(friendId);

            if (friendOptional.isPresent()) {
                if (!userOptional.get().getFriends().contains(friendOptional.get())) {
                    userOptional.get().getFriends().add(friendOptional.get());
                    return userRepository.save(userOptional.get());
                } else {
                    throw new AlreadyFriendsException("User: " + userId + " is already friends with user: " + friendId);
                }
            } else {
                throw new UserNotFoundException("Could not find userId: " +  userId);
            }
        } else {
            throw new UserNotFoundException("Could not find userId: " +  userId);
        }
    }

    @Override
    public List<User> getUserFriends(int userId) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isPresent()) {
            return userOptional.get().getFriends();
        } else {
            throw new UserNotFoundException("Could not find userId: " +  userId);
        }

    }

    @Override
    public User removeFriend(int userId, int friendId) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isPresent()) {
            Optional<User> friendOptional = userRepository.findById(friendId);

            if (friendOptional.isPresent()) {
                if (userOptional.get().getFriends().contains(friendOptional.get())) {
                    userOptional.get().getFriends().remove(friendOptional.get());
                    return userRepository.save(userOptional.get());
                } else {
                    throw new FriendNotFoundException("Could not find " + friendOptional.get().getUsername() + " in friendlist.");
                }
            } else {
                throw new UserNotFoundException("Could not find userId: " +  userId);
            }
        } else {
            throw new UserNotFoundException("Could not find userId: " +  userId);
        }
    }

    @Override
    public User getFriend(int userId, int friendId) throws UserNotFoundException, FriendNotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isPresent()) {
            Optional<User> optionalFriend = userOptional.get().getFriends().stream().filter(f -> f.getUserId().equals(friendId)).findFirst();

            if (optionalFriend.isPresent()) {
                return optionalFriend.get();
            } else throw new FriendNotFoundException("Could not find " + optionalFriend.get().getUsername() + " in friendlist.");
        } else {
            throw new UserNotFoundException("Could not find userId: " +  userId);
        }


    }
}
