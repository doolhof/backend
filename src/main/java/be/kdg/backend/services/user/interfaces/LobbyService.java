package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.LobbyNotFoundException;

import java.util.List;

public interface LobbyService {

    Lobby createLobby(List<User> users, GameSetting gameSetting);

    Lobby saveLobby(Lobby lobby);

    Lobby findLobbyById(int id) throws LobbyNotFoundException;

    List<Lobby> findLobbiesPerUser(int userId);

    void deleteLobby(Lobby lobby) throws LobbyNotFoundException;

    void sendInviteByEmail(List<User> users, int lobbyId) throws InterruptedException;

    void setLobbyHost(Lobby lobby, User host);

    void setGame(Lobby lobby, Game game);
}
