package be.kdg.backend.services.user.implementations.userStatistic;

import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.domain.user.UserStatistic;
import be.kdg.backend.persistence.interfaces.UserStatisticRepository;
import be.kdg.backend.services.user.interfaces.UserService;
import be.kdg.backend.services.user.interfaces.UserStatisticService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("UserStatisticService")
@Transactional
public class UserStatisticServiceImpl implements UserStatisticService {
    private final UserService userService;
    private final UserStatisticRepository userStatisticRepository;

    public UserStatisticServiceImpl(UserService userService, UserStatisticRepository userStatisticRepository) {
        this.userService = userService;
        this.userStatisticRepository = userStatisticRepository;
    }

    /**
     * this method updates all the user statistics at the end of the game
     *
     * @param player player who won the game
     */
    @Override
    public void UpdateStatistics(Player player) {
        List<Player> playerList = player.getGame().getPlayerList();
        for (int i = 0; i < playerList.size(); i++) {
            User user = userService.findUserByUsername(playerList.get(i).getName());
            if (playerList.get(i).getName() == player.getName()) {
                UserStatistic userStatistic = user.getUserStatistic();
                userStatistic.setGamesWon(userStatistic.getGamesWon() +1);
                userStatistic.setGamesLost(userStatistic.getGamesLost());
                userStatistic.setGamesPlayed(userStatistic.getGamesPlayed()+1);
                userStatisticRepository.save(userStatistic);
            } else {
                UserStatistic userStatistic = user.getUserStatistic();
                userStatistic.setGamesWon(userStatistic.getGamesWon());
                userStatistic.setGamesLost(userStatistic.getGamesLost()+1);
                userStatistic.setGamesPlayed(userStatistic.getGamesPlayed()+1);
                userStatisticRepository.save(userStatistic);
            }
        }
    }
}
