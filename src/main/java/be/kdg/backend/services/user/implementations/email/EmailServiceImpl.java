package be.kdg.backend.services.user.implementations.email;

import be.kdg.backend.domain.user.RegistrationToken;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.MailContentException;
import be.kdg.backend.exceptions.MailSenderException;
import be.kdg.backend.services.game.implementations.registrationToken.RegistrationTokenServiceImpl;
import be.kdg.backend.services.game.interfaces.RegistrationTokenService;
import be.kdg.backend.services.user.interfaces.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class EmailServiceImpl implements EmailService {
    private RegistrationTokenService registrationTokenService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationTokenServiceImpl.class);
    private final EmailServiceHelper emailServiceHelper;

    public EmailServiceImpl(RegistrationTokenService registrationTokenService, EmailServiceHelper emailServiceHelper) {
        this.registrationTokenService = registrationTokenService;
        this.emailServiceHelper = emailServiceHelper;
    }

    public MimeMessage sendSimpleMessage(String subject, String text, List<User> users, int lobbyId) throws MailContentException, MailSenderException {
        Session session = emailServiceHelper.createSession();
        String token = UUID.randomUUID().toString();
        StringBuilder emails = new StringBuilder();
        users.forEach(user -> {
            registrationTokenService.saveToken(new RegistrationToken(token, user));
            emails.append(user.getEmail()).append(",");
        });
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress("doolhofIP2@outlook.com", "NoReply-JD"));

            msg.setSubject(subject, "UTF-8");
            msg.setText(text, "UTF-8");
            msg.setSentDate(new Date());

            Multipart multipart = new MimeMultipart();
            BodyPart htmlPart = new MimeBodyPart();
            String htmlText = "";

            switch (subject) {
                case "Registration":
                    htmlText = emailServiceHelper.contentRegistration(token);
                    break;
                case "Invitation":
                    htmlText = emailServiceHelper.sendInvitation(lobbyId);
            }

            htmlPart.setContent(htmlText, "text/html");
            multipart.addBodyPart(htmlPart);
            htmlPart = new MimeBodyPart();
            DataSource fds = new FileDataSource("src\\main\\resources\\images\\LabyrinthLogo.jpg");

            htmlPart.setDataHandler(new DataHandler(fds));
            htmlPart.setHeader("Content-ID", "<image>");
            multipart.addBodyPart(htmlPart);
            msg.setContent(multipart);
            System.out.print(emails.toString());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emails.toString(), false));
            System.out.println("Message is ready");
           Transport.send(msg);


            System.out.println("EMail Sent Successfully!!");
            return msg;
        } catch (MessagingException e) {
            throw new MailContentException("error by creating content of mail");
            // LOGGER.info("error in mailservice");
            //return null;
        } catch (UnsupportedEncodingException e) {
            throw new MailSenderException("error by setting the sender of the mail");

        }
    }
}
