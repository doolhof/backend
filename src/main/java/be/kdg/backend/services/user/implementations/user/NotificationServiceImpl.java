package be.kdg.backend.services.user.implementations.user;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.domain.user.NotificationType;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.NotificationRepository;
import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.domain.user.NotificationType;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.NotificationRepository;
import be.kdg.backend.services.user.implementations.pushNotifications.PushNotificationService;
import be.kdg.backend.services.user.interfaces.NotificationService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("NotificationService")
@Transactional
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notifRepository;
    private final PushNotificationService pushNotificationService;

    public NotificationServiceImpl(NotificationRepository notificationRepository, PushNotificationService pushNotificationService) {
        this.notifRepository = notificationRepository;
        this.pushNotificationService = pushNotificationService;
    }

    @Override
    public Notification saveNotification(Notification notification) {
        return notifRepository.save(notification);
    }

    @Override
    public List<Notification> findAllByUserId(int userId) {
        return notifRepository.findAll().stream().filter(x -> x.getUser().getUserId() == userId && !x.isClosed()).collect(Collectors.toList());

    }

    @Override
    public void createForLobbyInvite(List<User> users, Lobby lobby) {
        for (User user : users) {
            Notification notification = new Notification(lobby.getHost().getUsername() + " has invited you for a lobby. ", user, NotificationType.LOBBYINVITE, lobby.getLobbyId()+"", null, false);
            saveNotification(notification);
            pushNotificationService.send(notification);
        }
    }

    @Override
    public void setToClosed(int id) {
        Optional<Notification> optionalnotif = notifRepository.findById(id);
        if (optionalnotif.isPresent()) {
                Notification notification = optionalnotif.get();
                notification.setClosed(true);
                saveNotification(notification);
        }
    }
}
