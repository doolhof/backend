package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.UserNotFoundException;

import java.util.List;

public interface UserService {
    User findUserById(int id) throws UserNotFoundException;

    User findUserByEmail(String email) throws UserNotFoundException;

    User findUserByUsername(String username) throws UserNotFoundException;

    List<User> findAllUsers();

    User saveUser(User user);

    void checkLogin(int userId, String currentPassword);

    void deleteUser(int userId) throws UserNotFoundException;

    void updatePassword(int userId, String oldPassword, String newPassword);

    String signIn(String username, String password);

    String signInFb(String username, String password);

    boolean checkUserNotExist(User user);

    void addLobby(User user, Lobby lobby);

    List<User> getCommunity(int userId, int min, int max);

    List<User> searchUsers(String search, int userId);

    User updateUser(User user) throws UserNotFoundException;
}
