package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.user.GameSetting;

public interface GameSettingService {

    GameSetting createGameSetting(int amountOfUsers, int secondsPerTurn);

    GameSetting save(GameSetting gameSetting);
}
