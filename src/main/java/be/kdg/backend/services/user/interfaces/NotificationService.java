package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.domain.user.Notification;
import be.kdg.backend.domain.user.User;

import java.util.List;

public interface NotificationService {
    Notification saveNotification(Notification notification);
    List<Notification> findAllByUserId(int userId);

    void createForLobbyInvite(List<User> users, Lobby lobby);
    void setToClosed(int id);


}
