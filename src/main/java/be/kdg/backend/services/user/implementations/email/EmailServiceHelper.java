package be.kdg.backend.services.user.implementations.email;

import be.kdg.backend.domain.user.User;
import org.springframework.stereotype.Component;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

@Component
public class EmailServiceHelper {

    //TODO Bad practice to write userName and Password like that
    public Session createSession() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.outlook.com");
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        return Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("doolhofIP2@outlook.com", "zhou-ie-zhou");
                    }
                });
    }

    //TODO href is based on local host, don't forget to change this if you're going to put this publicly
    public String contentRegistration(String token) {
        return
                "<body style=\"background-color:#1F1051;\">" + "<br/>" +
                        "<img src=\"cid:image\" style=\"display:block;margin:0 auto;\">" +
                        "<h1 style=\"color:#FF458D;text-align:center\">Click <a href=\"http://localhost:4200/register/confirm?token=" + token + "\" style=\"color:#FF458D;\">here</a>  to confirm your registration.</h1>";
    }

    public String sendInvitation( int lobbyId) {
        return
                "<body style=\"background-color:#1F1051;\">" + "<br/>" +
                        "<img src=\"cid:image\" style=\"display:block;margin:0 auto;\">" +
                        "<h1 style=\"color:#FF458D;text-align:center\">Click <a href=\"http://localhost:4200/lobby/" + lobbyId + "\" style=\"color:#FF458D;\">here</a>  to join the game.</h1>";

    }
}
