package be.kdg.backend.services.user.interfaces;

import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.MailContentException;
import be.kdg.backend.exceptions.MailSenderException;
import jdk.dynalink.linker.LinkerServices;

import javax.mail.internet.MimeMessage;
import java.util.List;

public interface EmailService {
    MimeMessage sendSimpleMessage(String subject, String text,List<User> users, int lobbyId) throws MailContentException, MailSenderException;
}
