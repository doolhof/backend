package be.kdg.backend.services.user.implementations.lobby;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.domain.user.Lobby;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.LobbyNotFoundException;
import be.kdg.backend.exceptions.MailContentException;
import be.kdg.backend.exceptions.MailSenderException;
import be.kdg.backend.persistence.interfaces.LobbyRepository;
import be.kdg.backend.services.user.interfaces.EmailService;
import be.kdg.backend.services.user.interfaces.LobbyService;
import be.kdg.backend.services.user.interfaces.UserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service("LobbyService")
@Transactional
public class LobbyServiceImpl implements LobbyService {
    private final LobbyRepository lobbyRepository;
    private UserService userService;
    private EmailService emailService;

    public LobbyServiceImpl(LobbyRepository lobbyRepository, UserService userService, EmailService emailService) {
        this.lobbyRepository = lobbyRepository;
        this.userService = userService;
        this.emailService = emailService;
    }

    @Override
    public Lobby createLobby(List<User> users, GameSetting gameSetting) {
        return new Lobby(users, gameSetting);
    }

    @Override
    public Lobby saveLobby(Lobby lobby) {
        return lobbyRepository.save(lobby);
    }

    @Override
    public Lobby findLobbyById(int id) throws LobbyNotFoundException {
        Optional<Lobby> optionalMazeCard = lobbyRepository.findById(id);
        if (optionalMazeCard.isPresent()) {
            return optionalMazeCard.get();
        } else {
            throw new LobbyNotFoundException(("Can't find lobby with id: " + id));
        }
    }

    @Override
    public List<Lobby> findLobbiesPerUser(int userId) {
        User user = userService.findUserById(userId);
        return user.getLobbies();
    }

    @Override
    public void deleteLobby(Lobby lobby) {
        lobbyRepository.delete(lobby);
    }

    @Override
    public void sendInviteByEmail(List<User> users, int lobbyId) throws MailContentException, MailSenderException {
        emailService.sendSimpleMessage("Invitation", "I want to invite you!", users, lobbyId);

    }

    @Override
    public void setLobbyHost(Lobby lobby, User host) {
        lobby.setHost(host);
    }

    @Override
    public void setGame(Lobby lobby, Game game) {
        lobby.setGame(game);
    }
}
