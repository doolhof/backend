package be.kdg.backend.services.user.implementations.user;

import be.kdg.backend.domain.user.*;
import be.kdg.backend.exceptions.CustomException;
import be.kdg.backend.exceptions.UserNotFoundException;
import be.kdg.backend.exceptions.UserServiceException;
import be.kdg.backend.persistence.interfaces.UserRepository;
import be.kdg.backend.security.JwtTokenProvider;
import be.kdg.backend.services.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private AuthenticationManager authenticationManager;
    private JwtTokenProvider jwtTokenProvider;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public User findUserById(int id) throws UserNotFoundException {
        Optional<User> u = userRepository.findById(id);
        if (u.isPresent()) {
            return u.get();
        } else {
            throw new UserNotFoundException("User with id: " + id + " not found.");
        }
    }

    @Override
    public User findUserByEmail(String email) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findByEmail(email);

        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new UserNotFoundException("User with email: " + email + " not found.");
        }
    }

    @Override
    public User findUserByUsername(String username) throws UserNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username);

        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new UserNotFoundException("User with username: " + username + " not found.");
        }
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User saveUser(User user) throws UserServiceException {
        if (!user.getEncryptedPassword().startsWith("uuidFb")){
            user.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getEncryptedPassword()));
        }
        if (user.getUserStatistic()==null) user.setUserStatistic(new UserStatistic(0,0,0));
        if (user.getUserSetting()==null) user.setUserSetting(new UserSetting(true,true,true,true,true));
        if (user.getLobbies() == null) {
            List<Lobby> lobbies = null;
            user.setLobbies(lobbies);
        }
        user.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT)));
        User u = userRepository.save(user);
        if (u == null)
            throw new UserServiceException(user.getUsername() + " not saved");
        return u;
    }


    @Override
    public void deleteUser(int userId) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent())
            throw new UserServiceException("User not found");

        userRepository.delete(user.get());
    }

    @Override
    public void checkLogin(int userId, String currentPassword) throws UserServiceException {
        Optional<User> u = userRepository.findById(userId);
        boolean checkPass = BCrypt.checkpw(currentPassword, u.get().getEncryptedPassword());
        if (u == null || !BCrypt.checkpw(currentPassword, u.get().getEncryptedPassword())) {
            throw new UserServiceException(("Wrong username or password " + userId));
        }
    }

    @Override
    public void updatePassword(int userId, String oldPassword, String newPassword) throws UserNotFoundException {
        Optional<User> u = userRepository.findById(userId);
        if (u.isPresent()) {
            checkLogin(userId, oldPassword);
            u.get().setEncryptedPassword(bCryptPasswordEncoder.encode(newPassword));
            userRepository.save(u.get());
        } else {
            throw new UserNotFoundException("User with id: " + userId + " not found.");
        }

    }

    @Override
    public String signIn(String username, String password)  {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).get().getRoles());
        } catch (AuthenticationException e) {
            throw new UserNotFoundException("Invalid username/password supplied");
        }
    }

    @Override
    public String signInFb(String username, String password) {
        try {
           // authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).get().getRoles());
        } catch (AuthenticationException e) {
            throw new UserNotFoundException("Invalid username/password supplied");
        }
    }


    @Override
    public boolean checkUserNotExist(User user) {
        List<User> users = userRepository.findAll();
        for (User u : users) {
            if (u.getEmail().equals(user.getEmail()) || u.getUsername().equals(user.getUsername())) {

                return false;
            }
        }
        return true;
    }

    @Override
    public List<User> getCommunity(int userId, int min, int max) {

        return userRepository.findUsers(userId, PageRequest.of(0, max));
    }

    @Override
    public List<User> searchUsers(String search, int userId) {
        return userRepository.findUsersByUsernameContainingIgnoreCaseAndUserIdNot(search, userId);
    }

    public User updateUser(User user) throws UserNotFoundException {
        Optional<User> originalUser = userRepository.findById(user.getUserId());
        if (originalUser.isPresent()) {
            originalUser.get().setUserSetting(user.getUserSetting());
            return userRepository.save(originalUser.get());
        } else {
            throw new UserNotFoundException("User with id: " + user.getUserId() + "not found.");
        }

    }

    @Override
    public void addLobby(User user, Lobby lobby) {
        user.getLobbies().add(lobby);

    }
}
