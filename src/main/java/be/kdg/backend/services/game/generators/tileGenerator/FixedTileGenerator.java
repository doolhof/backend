package be.kdg.backend.services.game.generators.tileGenerator;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Tile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for generating fixed tiles
 */
@Component
public class FixedTileGenerator {
    /**
     * This method is responsible for creating tiles you can't move, we're using the generateFixedPositions
     * method to get the positions, and for those positions we will give a mazeCard.
     *
     * @param gameBoard gameBoard for which you want to create fixed tiles
     * @param fixedMazeCards a list of all the fixed mazeCards
     * @param fixedPositionList a list of all the positions you have to add the fixed MazeCardsTo
     * @return a list of tiles with fixed mazeCards
     */
    public List<Tile> generateFixedTiles(List<Integer> fixedPositionList, GameBoard gameBoard, List<MazeCard> fixedMazeCards) {
        List<Tile> tiles = new ArrayList<>();
        List<MazeCard> fixedMazecardList = fixedMazeCards;
        for (int i = 1; i <= fixedPositionList.size(); i++) {
            Tile tile = new Tile(fixedPositionList.get(i - 1), false, fixedMazecardList.get(i - 1), gameBoard);
            tiles.add(tile);
        }
        return tiles;
    }

    /**
     * This method is responsible for getting a list with positions you can't move (the fixed tiles)
     */
    public List<Integer> generateFixedPositions() {
        List<Integer> fixedPositionList = new ArrayList<>();
        int startIndex = 1;
        for (int i = 1; i <= 4; i++) {
            for (int x = startIndex; x <= 7 + startIndex; x += 2) {
                fixedPositionList.add(x);
            }
            startIndex = startIndex + 14;
        }
        return fixedPositionList;
    }
}
