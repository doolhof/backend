package be.kdg.backend.services.game.implementations.game;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameServiceHelper {

    private final TreasureService treasureService;
    private final GameBoardService gameBoardService;
    private final EntityManager entityManager;
    public GameServiceHelper(TreasureService treasureService, GameBoardService gameBoardService, EntityManager entityManager) {
        this.treasureService = treasureService;
        this.gameBoardService = gameBoardService;
        this.entityManager = entityManager;
    }

    public GameBoard copyGameBoard(GameBoard gameBoard) {
        GameBoard updatedGameboard = new GameBoard();
        updatedGameboard.setTiles(gameBoard.getTiles().stream().map(tile -> new Tile(tile.getPosition(), tile.isMoveable(),
                new MazeCard(tile.getMazeCard().getOrientation(), tile.getMazeCard().isNorthWall(), tile.getMazeCard().isEastWall(),
                        tile.getMazeCard().isSouthWall(), tile.getMazeCard().isWestWall(),
                        tile.getMazeCard().getImageType(), copyTreasure(tile.getMazeCard().getTreasure())),
                updatedGameboard)).collect(Collectors.toList()));

        return gameBoardService.saveGameBoard(updatedGameboard);
    }

    public Treasure copyTreasure(Treasure treasure) {
        if (treasure == null) {
            return null;
        } else {
            return treasureService.save(new Treasure(treasure.getName(), treasure.getPlayerFind(), treasure.getPlayerFound(), treasure.getMazeCard()));
        }
    }

    public List<Player> copyPlayers(List<Player> players) {
        return new ArrayList<>(players.stream().map(player -> new Player(player.getColor(), player.getName(),
                player.getStartPosition(), player.getStartPosition(),
                new ArrayList<Treasure>(player.getToFind().stream().map(this::copyTreasure).collect(Collectors.toList())),
                new ArrayList<>(), null,
                null, false)).collect(Collectors.toList()));

    }
    public   MazeCard copyRemainingCard(MazeCard remainingCard){
        return   new MazeCard(remainingCard.getOrientation(), remainingCard.isNorthWall(), remainingCard.isEastWall(),remainingCard.isSouthWall(),
                remainingCard.isWestWall(),
                remainingCard.getImageType(), copyTreasure(remainingCard.getTreasure()));
    }
}

