package be.kdg.backend.services.game.generators.mazeCardGenerator;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Treasure;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * This class is responsible for generating mazeCards for when the game is created, these are fixed and the random
 * mazeCards, the mazeCards will receive a random orientation and treasures.
 */
@Component
public class MazeCardGenerator {
    private final MazeCardGeneratorHelper mazeCardGeneratorHelper;

    public MazeCardGenerator(MazeCardGeneratorHelper mazeCardGeneratorHelper) {
        this.mazeCardGeneratorHelper = mazeCardGeneratorHelper;
    }

    /**
     * This method gives back all the fixedMazeCards back
     *
     * @param treasures list of all the fixed mazeCard's treasures
     * @Return list of mazeCard for the tiles with a fixed position
     */
    public List<MazeCard> generateFixedMazeCards(List<Treasure> treasures) {
        List<MazeCard> fixedMazeCardList = new ArrayList<>();
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, 0, 1));//0 corner
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 90, 3, treasures.get(0)));//1
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 90, 3, treasures.get(1)));//2
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, 90, 1));//3 corner
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 0, 3, treasures.get(2)));//4
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 270, 3, treasures.get(3)));//5
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 180, 3, treasures.get(4)));//6
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 180, 3, treasures.get(5)));//7
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 0, 3, treasures.get(6)));//8
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 180, 3, treasures.get(7)));//9
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 90, 3, treasures.get(8)));//10
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 180, 3, treasures.get(9)));//11
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, 270, 1));//12 corner
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 270, 3, treasures.get(10)));//13
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, 270, 3, treasures.get(11)));//14
        fixedMazeCardList.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, 180, 1));//15 corner
        return fixedMazeCardList;
    }

    /**
     * This method is responsible for generating mazeCards for tiles that you can move, these are 12 straight,
     * 16 corners en 6 T-pose mazeCards. We give some of these mazeCards a treasure and shuffle them into a list
     *
     * @param treasures a list of treasures for moveAble MazeCards
     * @Return a list of shuffled mazeCards for moveAble tiles
     */
    public List<MazeCard> generateMoveableMazecards(List<Treasure> treasures) {
        List<MazeCard> mazeCards = new ArrayList<>();
        int treasureIndex = 0;

        for (int i = 0; i < 12; i++) {
            mazeCards.add(mazeCardGeneratorHelper.generateMazecard(false, true, false, true, mazeCardGeneratorHelper.getRandomOrientation(), 2));
        }

        for (int i = 0; i < 16; i++) {
            if (i < 6) {
                mazeCards.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, mazeCardGeneratorHelper.getRandomOrientation(), 1, treasures.get(treasureIndex)));
                treasureIndex++;
            } else {
                mazeCards.add(mazeCardGeneratorHelper.generateMazecard(true, false, false, true, mazeCardGeneratorHelper.getRandomOrientation(), 1));
            }
        }

        for (int i = 0; i < 6; i++) {
            mazeCards.add(mazeCardGeneratorHelper.generateMazecard(false, false, false, true, mazeCardGeneratorHelper.getRandomOrientation(), 3, treasures.get(treasureIndex)));
            treasureIndex++;
        }
        Collections.shuffle(mazeCards);
        return mazeCards;
    }


}
