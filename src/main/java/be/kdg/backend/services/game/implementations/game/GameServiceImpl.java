package be.kdg.backend.services.game.implementations.game;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.CreatePlayerException;
import be.kdg.backend.exceptions.GameNotFoundException;
import be.kdg.backend.exceptions.InvalidColorException;
import be.kdg.backend.exceptions.TooManyPlayersException;
import be.kdg.backend.persistence.interfaces.GameRepository;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service("GameService")
@Transactional
public class GameServiceImpl implements GameService {
    private final GameRepository gameRepository;
    private final TreasureService treasureService;
    private final GameBoardService gameBoardService;
    private final PlayerService playerService;
    private final GameServiceHelper gameServiceHelper;
    private final EntityManager entityManager;

    public GameServiceImpl(GameRepository gameRepository, TreasureService treasureService, GameBoardService gameBoardService, PlayerService playerService, GameServiceHelper gameServiceHelper, EntityManager entityManager) {
        this.gameRepository = gameRepository;
        this.treasureService = treasureService;
        this.gameBoardService = gameBoardService;
        this.playerService = playerService;
        this.gameServiceHelper = gameServiceHelper;
        this.entityManager = entityManager;
    }

    // This method creates a new game.
    @Override
    public Game createGame() {
        return saveGame(new Game());
    }

    //this method will return a game ready to be played.
    @Override
    public Game startGame(Game game, List<User> users, int turnTime) throws GameNotFoundException, TooManyPlayersException, CreatePlayerException {
            //check amount of players
            if (users.size() > 4) {
                throw new TooManyPlayersException("Too many players to start a game.");

            }

            //create and save new Treasure list
            List<Treasure> treasures = treasureService.generateTreasures();

            //create and save new Gameboard object
            game.setGameBoard(gameBoardService.createGameBoard(treasures));
                game.setStartGameBoard(gameServiceHelper.copyGameBoard(game.getGameBoard()));

            //create and save new Player objects and add them to players list
            List<Player> players = new ArrayList<>();

            Color[] colors = Color.values();
            int colorCounter = 0;

            for (User user : users) {
                try {
                    players.add(playerService.createPlayer(
                            colors[colorCounter],
                            user.getUsername(),
                            null,
                            game
                    ));
                    colorCounter++;
                } catch (InvalidColorException ice) {
                    throw new CreatePlayerException(ice.getMessage());
                }

            }
            // distributeTreasures(treasures, players);
            game.setPlayerList(playerService.saveAllPlayers(distributeTreasures(treasures, players)));

            //set game startdate and status
            game.setStartDate(LocalDateTime.now());

            game.setTurnTime(turnTime);
            game.setStatus(GameStatus.STARTED);
//            return saveGame(game);
            return game;
    }

    //shuffle and add treasures to all players
    private List<Player> distributeTreasures(List<Treasure> treasures, List<Player> players) {
        //determine amount of treasures for each player
        int treasuresPerPlayer = treasures.size() / players.size();
        int from = 0;
        int to = treasuresPerPlayer;
        //shuffle the treasures to make each game random
        Collections.shuffle(treasures);
        //distribute treasures for each player
        for (Player player : players) {
            player.setToFind(treasures.subList(from, to));
            List<Treasure> treasureList = treasures.subList(from, to);
            for (int i = 0; i < treasureList.size(); i++) {
                Treasure treasure = treasureList.get(i);
                treasure.setPlayerFind(player);
                treasureService.save(treasure);
            }
            from = to;
            to += treasuresPerPlayer;
        }
        return players;
    }

    @Override
    public Game getGameById(Integer gameId) throws GameNotFoundException {
        Optional<Game> optionalGame = gameRepository.findById(gameId);
        if (optionalGame.isPresent()) {
            return optionalGame.get();
        }
        throw new GameNotFoundException("Can't find game with id:" + gameId);
    }

    @Override
    public Game getGameForReplay(int gameId) {
        Game originalGame = getGameById(gameId);
        GameBoard updatedGameboard = gameServiceHelper.copyGameBoard(originalGame.getStartGameBoard());
        List<Player> copyPlayers = new ArrayList<Player>(gameServiceHelper.copyPlayers(originalGame.getPlayerList()));
        Game replayGame = new Game(LocalDateTime.now(), null, updatedGameboard,
                null,copyPlayers, originalGame.getTurnTime());
        copyPlayers.forEach(player -> player.setGame(replayGame));
        replayGame.setOriginalGame(originalGame.getGameId());
        playerService.saveAllPlayers(copyPlayers);
        entityManager.persist(replayGame);
        return gameRepository.save(replayGame);
    }

    @Override
    public Game saveGame(Game game) {
        return gameRepository.save(game);
    }

    @Override
    public List<Game> findAllGames() {
        return gameRepository.findAll();
    }

    @Override
    public void deleteGame(int gameId) {
        gameRepository.deleteById(gameId);
    }

}
