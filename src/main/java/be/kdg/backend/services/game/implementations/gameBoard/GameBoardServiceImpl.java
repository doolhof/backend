package be.kdg.backend.services.game.implementations.gameBoard;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Treasure;
import be.kdg.backend.exceptions.GameBoardServiceException;
import be.kdg.backend.exceptions.TooManyPlayersException;
import be.kdg.backend.persistence.interfaces.GameBoardRepository;
import be.kdg.backend.services.game.generators.tileGenerator.TileGenerator;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for creating a gameBoard and getting the gameBoard from the database
 */
@Service("GameBoardService")
@Transactional
public class GameBoardServiceImpl implements GameBoardService {
    private final GameBoardRepository gameBoardRepository;
    private final TileGenerator tileGenerator;
    private final GameBoardHelper gameBoardHelper;


    public GameBoardServiceImpl(GameBoardRepository gameBoardRepository, TileGenerator tileGenerator, GameBoardHelper gameBoardHelper) {
        this.gameBoardRepository = gameBoardRepository;
        this.tileGenerator = tileGenerator;
        this.gameBoardHelper = gameBoardHelper;
    }

    @Override
    public GameBoard createGameBoard(List<Treasure> treasures) {
        GameBoard gameBoard = new GameBoard();
        GameBoard gb = saveGameBoard(gameBoard);
        gb.setTiles(tileGenerator.generateTiles(gb, treasures));
        return gb;
    }

    @Override
    public GameBoard findGameBoard(int id) throws TooManyPlayersException {
        Optional<GameBoard> optionalGameBoard = gameBoardRepository.findById(id);
        if (optionalGameBoard.isPresent()) {
            return optionalGameBoard.get();
        }
        throw new GameBoardServiceException("Can't find gameBoard ");
    }

    @Override
    public GameBoard saveGameBoard(GameBoard gameBoard) {
        return gameBoardRepository.save(gameBoard);
    }

    @Override
    public int getDisabledArrow(int arrowNumber) {
        return gameBoardHelper.getDisableArrow(arrowNumber);
    }
}
