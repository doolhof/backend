package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Turn;
import be.kdg.backend.domain.user.Lobby;

import java.util.List;

public interface TurnService {
    Turn endTurn(int turnDuration,
                 String turnPhase,
                 int posMazecardMove,
                 List<Integer> playerMoves,
                 Integer playerId,
                 int orientation,
                 String playerName);
    List<Turn> getAllTurnsByGameId(int gameId);
}
