package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Tile;

import java.util.List;

/**
 * This is the interface for TileService
 */
public interface TileService {
    Tile findTileById(int tileId);

    Tile findTileByPosition(GameBoard gameBoard, int position);

    Tile saveTile(Tile tile);

    MazeCard findMazeCardWithoutTile(int gameBoardId);

    void pushMazeCard(int position, int gameBoardId);

}
