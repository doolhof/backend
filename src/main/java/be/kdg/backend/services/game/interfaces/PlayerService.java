package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.exceptions.InvalidMoveException;
import org.aspectj.apache.bcel.util.Play;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public interface PlayerService {

    Player createPlayer(Color color, String name, List<Treasure> toFind, Game game);

    Player getPlayerByUserId(int playerId);

    LinkedList<Tile> move(Player player, Tile destinationTile) throws InvalidMoveException;

    List<Player> allPlayersByGameId(Integer gameId);

    Player savePlayer(Player player);

    List<Player> saveAllPlayers(List<Player> players);

    List<Player> savePushedPosition(Player player, int position);

    Player updatePlayerTreasure(Tile tile, Player player);

    Boolean winGame(Player player);

}
