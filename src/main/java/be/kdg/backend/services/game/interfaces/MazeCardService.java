package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.MazeCard;

import java.util.List;

/**
 * This is the interface for MazeCardService
 */
public interface MazeCardService {
    MazeCard findMazeCardById(int id);

    MazeCard saveMazeCard(MazeCard mazeCard);

    MazeCard changeOrientation(MazeCard mazeCard, int degreesTurned);
}
