package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.user.RegistrationToken;

public interface RegistrationTokenService {
    RegistrationToken saveToken(RegistrationToken token);
    void confirmToken(String token);
    void deleteRegistrationToken(RegistrationToken token);
    RegistrationToken findById(int id);
}
