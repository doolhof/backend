package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.exceptions.TooManyPlayersException;

import java.util.List;

public interface GameService {

    Game createGame();

    Game startGame(Game game, List<User> users, int turnTime) throws TooManyPlayersException;

    Game getGameById(Integer gameId);

    Game saveGame(Game game);

    List<Game> findAllGames();

    Game getGameForReplay(int gameId);

    void deleteGame(int gameId);
}
