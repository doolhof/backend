package be.kdg.backend.services.game.generators.treasureGenerator;

import be.kdg.backend.domain.game.Treasure;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * This class is responsible for generating treasures
 */
@Component
public class TreasureGenerator {

    public List<Treasure> generateTreasures() {
        List<Treasure> treasures = new LinkedList<>();

        //fixed treasures (12)
        treasures.add(new Treasure("earth"));
        treasures.add(new Treasure("mars"));
        treasures.add(new Treasure("planet_4"));
        treasures.add(new Treasure("saturn"));
        treasures.add(new Treasure("sun"));
        treasures.add(new Treasure("moon"));
        treasures.add(new Treasure("planet_3"));
        treasures.add(new Treasure("planet"));
        treasures.add(new Treasure("planet_1"));
        treasures.add(new Treasure("black_hole"));
        treasures.add(new Treasure("planet_2"));
        treasures.add(new Treasure("supernova"));

        //random treasures (12)
        treasures.add(new Treasure("asteroid"));
        treasures.add(new Treasure("monster_1"));
        treasures.add(new Treasure("monster_2"));
        treasures.add(new Treasure("monster_3"));
        treasures.add(new Treasure("monster_4"));
        treasures.add(new Treasure("monster_5"));
        treasures.add(new Treasure("monster_6"));
        treasures.add(new Treasure("monster_7"));
        treasures.add(new Treasure("monster_8"));
        treasures.add(new Treasure("monster_9"));
        treasures.add(new Treasure("astronaut"));
        treasures.add(new Treasure("star"));
        return treasures;
    }

}