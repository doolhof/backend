package be.kdg.backend.services.game.implementations.mazeCard;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.exceptions.MazeCardServiceException;
import be.kdg.backend.persistence.interfaces.MazeCardRepository;
import be.kdg.backend.services.game.interfaces.MazeCardService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This class is responsible for getting mazeCards from the database
 */
@Service("MazeCardService")
@Transactional
public class MazeCardServiceImpl implements MazeCardService {
    private final MazeCardRepository mazeCardRepository;
    private final MazeCardServiceHelper mazeCardServiceHelper;

    public MazeCardServiceImpl(MazeCardRepository mazeCardRepository, MazeCardServiceHelper mazeCardServiceHelper) {
        this.mazeCardRepository = mazeCardRepository;
        this.mazeCardServiceHelper = mazeCardServiceHelper;
    }

    /**
     * This method changes the walls on a mazeCard based on the degrees turned (orientation)
     *
     * @param mazeCard      the mazeCard you want the walls to change of
     * @param degreesTurned the amount of degrees the mazeCard should be turned (orientation)
     * @return a mazeCard with the parameters of the walls changed based on the degreesTurned
     */
    public MazeCard changeOrientation(MazeCard mazeCard, int degreesTurned) {
        mazeCard = mazeCardServiceHelper.resetWalls(mazeCard);
        Map<Integer, Boolean> walls = Map.of(0, mazeCard.isNorthWall(), 90, mazeCard.isEastWall(), 180, mazeCard.isSouthWall(), 270, mazeCard.isWestWall());
        Map<Integer, Boolean> newWalls = new HashMap<>();
        for (Integer integer : walls.keySet()) {
            int newKey = (integer + degreesTurned) % 360;
            newWalls.put(newKey, walls.get(integer));
        }
        mazeCard.setOrientation(degreesTurned);
        mazeCard.setNorthWall(newWalls.get(0));
        mazeCard.setEastWall(newWalls.get(90));
        mazeCard.setSouthWall(newWalls.get(180));
        mazeCard.setWestWall(newWalls.get(270));
        return mazeCard;
    }

    @Override
    public MazeCard findMazeCardById(int id) throws MazeCardServiceException {
        Optional<MazeCard> optionalMazeCard = mazeCardRepository.findById(id);
        if (optionalMazeCard.isPresent()) {
            return optionalMazeCard.get();
        }
        throw new MazeCardServiceException("Can't find MazeCard ");
    }


    @Override
    public MazeCard saveMazeCard(MazeCard mazeCard) {
        return mazeCardRepository.save(mazeCard);
    }
}
