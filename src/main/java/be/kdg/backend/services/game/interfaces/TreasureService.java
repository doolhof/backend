package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.game.Treasure;

import java.util.List;

public interface TreasureService {
    List<Treasure> findAllTreasures();

    Treasure save(Treasure treasure);

    List<Treasure> getFixedTreasures(List<Treasure> treasures);

    List<Treasure> getRandomTreasures(List<Treasure> treasures);

    List<Treasure> generateTreasures();

    List<Treasure> saveTreasures(List<Treasure> treasures);
}
