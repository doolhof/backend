package be.kdg.backend.services.game.interfaces;

import be.kdg.backend.domain.user.GameSetting;

public interface GameSettingService {
    GameSetting saveGameSetting(GameSetting gameSetting);
}
