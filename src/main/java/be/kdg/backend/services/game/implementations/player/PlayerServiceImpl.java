package be.kdg.backend.services.game.implementations.player;

import be.kdg.backend.domain.game.*;
import be.kdg.backend.exceptions.InvalidColorException;
import be.kdg.backend.exceptions.InvalidMoveException;
import be.kdg.backend.persistence.interfaces.PlayerRepository;
import be.kdg.backend.services.game.generators.playerGenerator.PlayerGenerator;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import be.kdg.backend.services.user.interfaces.UserStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {
    private final PlayerRepository playerRepository;
    private final TileService tileService;
    private final PlayerMoveTileHelper playerMoveTileHelper;
    private final PlayerMovementHelper playerMovementHelper;
    private final PlayerTreasureHelper playerTreasureHelper;
    private final PlayerGenerator playerGenerator;
    private final UserStatisticService userStatisticService;
    private final static Logger LOGGER = Logger.getLogger(PlayerServiceImpl.class.getName());

    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository, TileService tileService, PlayerMoveTileHelper playerMoveTileHelper, PlayerMovementHelper playerMovementHelper, PlayerGenerator playerGenerator, PlayerTreasureHelper playerTreasureHelper, UserStatisticService userStatisticService) {
        this.playerRepository = playerRepository;
        this.tileService = tileService;
        this.playerMoveTileHelper = playerMoveTileHelper;
        this.playerMovementHelper = playerMovementHelper;
        this.playerTreasureHelper = playerTreasureHelper;
        this.playerGenerator = playerGenerator;
        this.userStatisticService = userStatisticService;
    }

    @Override
    public Player getPlayerByUserId(int playerId) {
        Optional<Player> player = playerRepository.findById(playerId);
        return player.orElse(null);
    }

    /**
     * This method will return the shortest path to a player's destination
     *
     * @param player          the player you want to search the path of
     * @param destinationTile the tile the player want to move to
     * @return list of tiles that the players has to go through to reach it's destination
     */
    @Override
    public LinkedList<Tile> move(Player player, Tile destinationTile) throws InvalidMoveException {
        Tile currentPositionTile = tileService.findTileByPosition(player.getGame().getGameBoard(), player.getCurrentPosition());
        LinkedList<LinkedList<Tile>> allPaths = playerMovementHelper.calculatePaths(currentPositionTile, destinationTile, currentPositionTile.getGameBoard());

        if (allPaths.size() != 0) {
            player.setCurrentPosition(destinationTile.getPosition());
            playerRepository.save(player);

            LinkedList<Tile> shortPath = new LinkedList<>(allPaths.get(0));
            for (LinkedList<Tile> path : allPaths) {
                if (path.size() < shortPath.size()) {
                    shortPath = path;
                }
            }
            return shortPath;
        } else {
            LOGGER.info("no path found");
            throw new InvalidMoveException("Invalid move");
        }
    }


    /**
     * this method is responsible for checking if a player has won the game, if the player is standing
     * on his start tile, with all his treasures found
     *
     * @param player the player that just moved
     * @return a boolean true, if the player has won the game
     */
    @Override
    public Boolean winGame(Player player) {
        if (player.getToFind().size() == 0 && player.getCurrentPosition() == player.getStartPosition()) {
            userStatisticService.UpdateStatistics(player);
            return true;
        }
        return false;
    }

    /**
     * This method places a treasure to the found list if the player is standing on the tile
     *
     * @param tile   the tile, the player is currently standing on
     * @param player the player that just moved
     * @return a updated player
     */
    @Override
    public Player updatePlayerTreasure(Tile tile, Player player) {
        player = playerTreasureHelper.foundTreasure(tile, player);
        return savePlayer(player);
    }

    @Override
    public List<Player> allPlayersByGameId(Integer gameId) {
        return playerRepository.findAll().stream().filter(x -> x.getGame().getGameId() == gameId).collect(Collectors.toList());
    }

    @Override
    public Player createPlayer(Color color, String name, List<Treasure> toFind, Game game) throws InvalidColorException {
        return playerGenerator.generatePlayer(color, name, toFind, game);
    }

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public List<Player> saveAllPlayers(List<Player> players) {
        return playerRepository.saveAll(players);
    }

    /**
     * This method saves the pushed position
     */
    @Override
    public List<Player> savePushedPosition(Player player, int position) {
        return saveAllPlayers(playerMoveTileHelper.moveTileWithPlayer(player, position));
        // return allPlayersByGameId(players.get(0).getGame().getGameId());
    }
}
