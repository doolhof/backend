package be.kdg.backend.services.game.implementations.gameSetting;

import be.kdg.backend.domain.user.GameSetting;
import be.kdg.backend.persistence.interfaces.GameSettingRepository;
import be.kdg.backend.services.game.interfaces.GameSettingService;

public class GameSettingServiceImpl implements GameSettingService {
    private final GameSettingRepository gameSettingRepository;

    public GameSettingServiceImpl(GameSettingRepository gameSettingRepository) {
        this.gameSettingRepository = gameSettingRepository;
    }

    @Override
    public GameSetting saveGameSetting(GameSetting gameSetting) {
        return gameSettingRepository.save(gameSetting);
    }
}
