package be.kdg.backend.services.game.implementations.mazeCard;

import be.kdg.backend.domain.game.MazeCard;
import org.springframework.stereotype.Component;

/**
 * This class is responsible for reseting the walls on a mazecard
 */
@Component
public class MazeCardServiceHelper {
    /**
     * This method resets the walls on a mazeCard to orientation 0
     *
     * @param mazeCard the mazeCard you want to reset the walls from
     * @return the mazeCard with the walls of orientation 0
     */
    public MazeCard resetWalls(MazeCard mazeCard) {
        if (mazeCard.getImageType() == 1) {
            mazeCard.setNorthWall(true);
            mazeCard.setEastWall(false);
            mazeCard.setSouthWall(false);
            mazeCard.setWestWall(true);
        } else if (mazeCard.getImageType() == 2) {
            mazeCard.setNorthWall(false);
            mazeCard.setEastWall(true);
            mazeCard.setSouthWall(false);
            mazeCard.setWestWall(true);
        } else if (mazeCard.getImageType() == 3) {
            mazeCard.setNorthWall(false);
            mazeCard.setEastWall(false);
            mazeCard.setSouthWall(false);
            mazeCard.setWestWall(true);
        }
        return mazeCard;
    }
}
