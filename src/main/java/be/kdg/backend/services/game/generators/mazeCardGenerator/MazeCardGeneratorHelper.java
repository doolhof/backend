package be.kdg.backend.services.game.generators.mazeCardGenerator;

import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Treasure;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * This class is responsible for generating a mazeCard
 */
@Component
public class MazeCardGeneratorHelper {
    /**
     * This method is responsible for generating a mazeCard and it will change the walls based on it's position
     *
     * @param north       the boolean for the northern wall
     * @param east        the boolean for the eastern wall
     * @param south       the boolean for the southern wall
     * @param west        the boolean for the western wall
     * @param orientation the orientation of how many degrees the mazeCard should be turned.
     * @param imageType   1 = corner, 2 = straight, 3 = T-pose
     * @return a mazeCard with changed walls based on the orientation
     */
    // TODO: 21-Feb-19 Map te zware datastructuur, later aanpassen
    public MazeCard generateMazecard(boolean north, boolean east, boolean south, boolean west, int orientation, int imageType) {
        Map<Integer, Boolean> walls = Map.of(0, north, 90, east, 180, south, 270, west);
        Map<Integer, Boolean> newWalls = new HashMap<>();
        for (Integer integer : walls.keySet()) {
            int newKey = (integer + orientation) % 360;
            newWalls.put(newKey, walls.get(integer));
        }
        return new MazeCard(orientation, newWalls.get(0), newWalls.get(90), newWalls.get(180), newWalls.get(270), imageType);
    }

    public MazeCard generateMazecard(boolean north, boolean east, boolean south, boolean west, int orientation, int imageType, Treasure treasure) {
        MazeCard mazeCard = generateMazecard(north, east, south, west, orientation, imageType);
        mazeCard.setTreasure(treasure);
        return mazeCard;
    }

    public int getRandomOrientation() {
        int[] orientations = {0, 90, 180, 270};
        int random = new Random().nextInt(orientations.length);
        return orientations[random];
    }
}
