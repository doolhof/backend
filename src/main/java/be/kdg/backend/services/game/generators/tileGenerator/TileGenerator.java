package be.kdg.backend.services.game.generators.tileGenerator;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.domain.game.Treasure;
import be.kdg.backend.services.game.generators.mazeCardGenerator.MazeCardGenerator;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class is responsible for generating all the tiles on the GameBoard
 */
@Component
public class TileGenerator {
    private final MazeCardGenerator mazeCardGenerator;
    private final TreasureService treasureService;
    private final FixedTileGenerator fixedTileGenerator;

    public TileGenerator(MazeCardGenerator mazeCardGenerator, TreasureService treasureService, FixedTileGenerator fixedTileGenerator) {
        this.mazeCardGenerator = mazeCardGenerator;
        this.treasureService = treasureService;
        this.fixedTileGenerator = fixedTileGenerator;
    }

    /**
     * This method is responsible for creating all the tiles and give them a mazeCard
     * we give the non-fixed position tiles a random maze card and than we will add the fixed positions tiles
     * with the generateFixedTiles() method. Finally we will sort the list we just created.
     *
     * @param gameBoard the gameBoard for which the tiles should be created
     * @return a list with all the tiles (with mazeCards)
     */
    public List<Tile> generateTiles(GameBoard gameBoard, List<Treasure> treasures) {
        List<Tile> tiles = new ArrayList<>();
        List<Integer> fixedPostitions = fixedTileGenerator.generateFixedPositions();
        List<Tile> fixedTiles = fixedTileGenerator.generateFixedTiles(fixedPostitions, gameBoard, mazeCardGenerator.generateFixedMazeCards(treasureService.getFixedTreasures(treasures)));
        List<MazeCard> moveableMazecards = mazeCardGenerator.generateMoveableMazecards(treasureService.getRandomTreasures(treasures));

        int moveableIndex = 0;
        for (int i = 1; i < 51; i++) {
            if (!fixedPostitions.contains(i)) {
                Tile tile = new Tile(i, true, moveableMazecards.get(moveableIndex), gameBoard);
                tiles.add(tile);
                moveableIndex++;
            }
        }
        tiles.addAll(fixedTiles);
        Collections.sort(tiles);
        return tiles;
    }
}
