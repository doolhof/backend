package be.kdg.backend.services.game.implementations.player;

import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.domain.game.Treasure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * This class is reponsible for checking if a player has found a treasure
 */
//TODO EXCEPTION HANDLING AFMAKEN
@Component
public class PlayerTreasureHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerTreasureHelper.class);

    public Player foundTreasure(Tile tile, Player player) {
        if (tile.getMazeCard().getTreasure() != null) {
            if (player.getToFind().get(0).getName().equals(tile.getMazeCard().getTreasure().getName())) {
                player.getToFind().get(0).setPlayerFind(null);
                player.getToFind().get(0).setPlayerFound(player);
                return player;
            }
        }
        return player;
    }
}
