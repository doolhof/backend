package be.kdg.backend.services.game.implementations.registrationToken;

import be.kdg.backend.domain.user.RegistrationToken;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.persistence.interfaces.RegistrationTokenRepository;
import be.kdg.backend.services.game.interfaces.RegistrationTokenService;
import be.kdg.backend.services.user.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RegistrationTokenServiceImpl implements RegistrationTokenService {
    private final RegistrationTokenRepository tokenRepository;
    private final UserService userService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationTokenServiceImpl.class);

    public RegistrationTokenServiceImpl(RegistrationTokenRepository tokenRepository, UserService userService) {
        this.tokenRepository = tokenRepository;
        this.userService = userService;
    }

    @Override
    public RegistrationToken saveToken(RegistrationToken token) {
        return tokenRepository.save(token);
    }

    @Override
    public void confirmToken(String token) {
        List<RegistrationToken> tokenList = tokenRepository.findAll().stream().filter(e -> e.getToken().equals(token)).collect(Collectors.toList());
        System.out.println(tokenList.size());
        User user = tokenList.get(0).getUser();
        user.setConfirmed(true);
        LOGGER.info("set user confirmed true");
        userService.saveUser(user);
        deleteRegistrationToken(tokenList.get(0));


    }

    @Override
    public void deleteRegistrationToken(RegistrationToken token) {
        tokenRepository.delete(token);
    }

    @Override
    public RegistrationToken findById(int id) {
        return tokenRepository.findById(id).get();
    }
}
