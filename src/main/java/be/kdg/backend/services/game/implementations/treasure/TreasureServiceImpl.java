package be.kdg.backend.services.game.implementations.treasure;

import be.kdg.backend.domain.game.Treasure;
import be.kdg.backend.persistence.interfaces.TreasureRepository;
import be.kdg.backend.services.game.generators.treasureGenerator.TreasureGenerator;
import be.kdg.backend.services.game.interfaces.TreasureService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("TreasureService")
@Transactional
public class TreasureServiceImpl implements TreasureService {
    private final TreasureRepository treasureRepository;
    private final TreasureGenerator treasureGenerator;

    public TreasureServiceImpl(TreasureRepository treasureRepository, TreasureGenerator treasureGenerator) {
        this.treasureRepository = treasureRepository;
        this.treasureGenerator = treasureGenerator;
    }

    @Override
    public List<Treasure> findAllTreasures() {
        return treasureRepository.findAll();
    }

    public Treasure save(Treasure treasure) {
        return treasureRepository.save(treasure);
    }

    public List<Treasure> generateTreasures() {
        return treasureRepository.saveAll(treasureGenerator.generateTreasures());
    }

    public List<Treasure> getFixedTreasures(List<Treasure> treasures) {
        return treasures.subList(0, 12);
    }

    public List<Treasure> getRandomTreasures(List<Treasure> treasures) {
        List<Treasure> randomTreasures = new ArrayList<>(treasures.subList(12, treasures.size()));
        Collections.shuffle(randomTreasures);
        return randomTreasures;
    }

    public List<Treasure> saveTreasures(List<Treasure> treasures) {
        return treasureRepository.saveAll(treasures);
    }
}
