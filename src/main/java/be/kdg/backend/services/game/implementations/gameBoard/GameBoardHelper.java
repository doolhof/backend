package be.kdg.backend.services.game.implementations.gameBoard;

import org.springframework.stereotype.Component;

@Component
public class GameBoardHelper {
    /**
     * This method will disable the arrow on the gameBoard on the opposite side
     *
     * @param number the number of the arrow the user just pushed
     * @return the number of the arrow on the opposite side
     */
    public int getDisableArrow(int number) {
        if (number == 2 || number == 4 || number == 6) {
            number += 42;
            return number;
        } else if (number == 14 || number == 28 || number == 42) {
            number -= 6;
            return number;
        } else if (number == 8 || number == 22 || number == 36) {
            number += 6;
            return number;
        } else if (number == 44 || number == 46 || number == 48) {
            number -= 42;
            return number;
        }
        return number;
    }
}
