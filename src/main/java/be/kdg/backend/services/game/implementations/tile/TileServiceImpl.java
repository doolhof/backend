package be.kdg.backend.services.game.implementations.tile;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.MazeCard;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.exceptions.TileServiceException;
import be.kdg.backend.persistence.interfaces.TileRepository;
import be.kdg.backend.services.game.interfaces.GameBoardService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TileService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for getting tiles from the database
 */
@Service("TileService")
@Transactional
public class TileServiceImpl implements TileService {
    private final TileRepository tileRepository;
    private final GameBoardService gameBoardService;


    public TileServiceImpl(TileRepository tileRepository, GameBoardService gameBoardService) {
        this.tileRepository = tileRepository;
        this.gameBoardService = gameBoardService;
    }

    @Override
    public Tile findTileById(int tileId) throws TileServiceException {
        Optional<Tile> optionalTile = tileRepository.findById(tileId);
        if (optionalTile.isPresent()) {
            return optionalTile.get();
        }
        throw new TileServiceException("Can't find Tile ");
    }

    @Override
    public Tile findTileByPosition(GameBoard gameBoard, int position) throws TileServiceException {
        Optional<Tile> optionalTile = Optional.ofNullable(tileRepository.findTileByGameBoardAndPosition(gameBoard, position));
        if (optionalTile.isPresent()) {
            return optionalTile.get();
        }
        throw new TileServiceException("Can't find the tile ");
    }

    @Override
    public Tile saveTile(Tile tile) {
        return tileRepository.save(tile);
    }


    /**
     * This method finds the single mazeCard that's not in the maze
     */
    public MazeCard findMazeCardWithoutTile(int gameBoardId) throws TileServiceException {
        Optional<MazeCard> optionalMazeCard = Optional.ofNullable(gameBoardService.findGameBoard(gameBoardId).getTiles().get(49).getMazeCard());
        if (optionalMazeCard.isPresent()) {
            return optionalMazeCard.get();
        }
        throw new TileServiceException("Can't find the remaining mazeCard ");
    }

    /**
     * This method pushes a specific row/column of tiles towards a direction based on the arrow clicked
     *
     * @param position    the arrow the players has pushed on
     * @param gameboardId the id of the game the player is playing on
     */
    @Override
    public void pushMazeCard(int position, int gameboardId) {
        List<Tile> tiles = gameBoardService.findGameBoard(gameboardId).getTiles();
        MazeCard temporaryMazeCard = findMazeCardWithoutTile(gameboardId);
        MazeCard nextMazeCard;

        //TOP
        if (position == 2 || position == 4 || position == 6) {
            for (int i = position; i <= position + 42; i += 7) {
                nextMazeCard = tiles.get(i - 1).getMazeCard();
                tiles.get(i - 1).setMazeCard(temporaryMazeCard);
                saveTile(tiles.get(i - 1));
                temporaryMazeCard = nextMazeCard;
            }
            Tile tile = tiles.get(50 - 1);
            tile.setMazeCard(temporaryMazeCard);
            saveTile(tile);
        }
        //DOWN
        else if (position == 44 || position == 46 || position == 48) {
            for (int i = position; i >= position - 42; i -= 7) {
                nextMazeCard = tiles.get(i - 1).getMazeCard();
                tiles.get(i - 1).setMazeCard(temporaryMazeCard);
                saveTile(tiles.get(i - 1));
                temporaryMazeCard = nextMazeCard;
            }
            Tile tile = tiles.get(50 - 1);
            tile.setMazeCard(temporaryMazeCard);
            saveTile(tile);
        }
        //LEFT
        else if (position == 8 || position == 22 || position == 36) {
            for (int i = position; i <= position + 6; i++) {
                nextMazeCard = tiles.get(i - 1).getMazeCard();
                tiles.get(i - 1).setMazeCard(temporaryMazeCard);
                saveTile(tiles.get(i - 1));
                temporaryMazeCard = nextMazeCard;
            }
            Tile tile = tiles.get(50 - 1);
            tile.setMazeCard(temporaryMazeCard);
            saveTile(tile);
        }
        //RIGHT
        else if (position == 14 || position == 28 || position == 42) {
            for (int i = position; i >= position - 6; i--) {
                nextMazeCard = tiles.get(i - 1).getMazeCard();
                tiles.get(i - 1).setMazeCard(temporaryMazeCard);
                saveTile(tiles.get(i - 1));
                temporaryMazeCard = nextMazeCard;
            }
            Tile tile = tiles.get(50 - 1);
            tile.setMazeCard(temporaryMazeCard);
            saveTile(tile);
        }
    }
}
