package be.kdg.backend.services.game.implementations.turn;

import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Turn;
import be.kdg.backend.domain.game.TurnPhase;
import be.kdg.backend.persistence.interfaces.TurnRepository;
import be.kdg.backend.services.game.interfaces.GameService;
import be.kdg.backend.services.game.interfaces.PlayerService;
import be.kdg.backend.services.game.interfaces.TurnService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service("TurnService")
@Transactional
public class TurnServiceImpl implements TurnService {
    private final TurnRepository turnRepository;
    private final PlayerService playerService;
    private final GameService gameService;

    public TurnServiceImpl(TurnRepository turnRepository, PlayerService playerService, GameService gameService, GameService gameService1) {
        this.turnRepository = turnRepository;
        this.playerService = playerService;
        this.gameService = gameService1;
    }

    @Override
    public Turn endTurn(int turnDuration,
                        String turnPhase,
                        int posMazecardMove,
                        List<Integer> playerMoves,
                        Integer playerId,
                        int orientation,
                        String playerName) {
        Player player = playerService.getPlayerByUserId(playerId);
        Turn turn = new Turn(turnDuration, TurnPhase.valueOf(turnPhase), LocalDateTime.now(), posMazecardMove, playerMoves,player,playerName , orientation);
        return turnRepository.save(turn);
    }

    @Override
    public List<Turn> getAllTurnsByGameId(int gameId) {
        return turnRepository.findTurnsByPlayerGame_Game(gameService.getGameById(gameId));
    }
}
