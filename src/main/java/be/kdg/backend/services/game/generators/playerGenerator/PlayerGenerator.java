package be.kdg.backend.services.game.generators.playerGenerator;

import be.kdg.backend.domain.game.Color;
import be.kdg.backend.domain.game.Game;
import be.kdg.backend.domain.game.Player;
import be.kdg.backend.domain.game.Treasure;
import be.kdg.backend.exceptions.InvalidColorException;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class PlayerGenerator {

    public Player generatePlayer(Color color, String name, List<Treasure> toFind, Game game) throws InvalidColorException{
        if (color != null) {
            int startPosition = 1;
            switch (color) {
                case DODGERBLUE:
                    startPosition = 7;
                    break;
                case RED:
                    startPosition = 43;
                    break;
                case GREEN:
                    startPosition = 1;
                    break;
                case YELLOW:
                    startPosition = 49;
                    break;
            }
            return new Player(color, name, startPosition, startPosition, toFind, new LinkedList<Treasure>(), new LinkedList<>(), game, false);
        } else {
            throw new InvalidColorException("Color is null.");
        }
    }
}
