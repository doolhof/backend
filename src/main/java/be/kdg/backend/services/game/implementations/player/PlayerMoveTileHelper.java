package be.kdg.backend.services.game.implementations.player;

import be.kdg.backend.domain.game.Player;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for moving a player if the tiles are pushed or if he's pushed of the board
 */
@Component
public class PlayerMoveTileHelper {

    /**
     * This method will return a player with his updated current location if he is on a tile that's been pushed
     *
     * @param myPlayer   this is the player you want to update
     * @param position this is the arrow position a users clicks on
     * @return the player with his updated position
     */
    public List<Player> moveTileWithPlayer(Player myPlayer, int position) {
        List<Integer> potentialPositions = getPotentialPositions(position);
        List<Player> players = myPlayer.getGame().getPlayerList();
        List<Player> playerList = new ArrayList<>();
        for (Player player : players) {
            if (potentialPositions.contains(player.getCurrentPosition())) {
                player = changePositionNormal(player, position);
                playerList.add(player);
            } else {
                player = changePositionEdge(player, position);
                playerList.add(player);
            }
        }
        return playerList;
    }

    /**
     * This method will change the player's current position if the player is pushed of the edge.
     */
    private Player changePositionEdge(Player player, int position) {
        if (position == 2 || position == 4 || position == 6) {
            if (player.getCurrentPosition() == position + 42) {
                player.setCurrentPosition(position);
            }
        } else if (position == 44 || position == 46 || position == 48) {
            if (player.getCurrentPosition() == position - 42) {
                player.setCurrentPosition(position);
            }
        } else if (position == 8 || position == 22 || position == 36) {
            if (player.getCurrentPosition() == position + 6) {
                player.setCurrentPosition(position);
            }
        } else if (position == 14 || position == 28 || position == 42) {
            if (player.getCurrentPosition() == position - 6) {
                player.setCurrentPosition(position);
            }
        }
        return player;
    }

    /**
     * This method will change the player's current position based on the tile he's on (without pushing the player off the maze)
     */
    private Player changePositionNormal(Player player, int position) {
        if (position == 2 || position == 4 || position == 6) {
            player.setCurrentPosition(player.getCurrentPosition() + 7);
        } else if (position == 44 || position == 46 || position == 48) {
            player.setCurrentPosition(player.getCurrentPosition() - 7);
        } else if (position == 8 || position == 22 || position == 36) {
            player.setCurrentPosition(player.getCurrentPosition() + 1);
        } else if (position == 14 || position == 28 || position == 42) {
            player.setCurrentPosition(player.getCurrentPosition() - 1);
        }
        return player;
    }


    /**
     * This method will return a list of potential places where a person can move to based on the clicked arrow
     * this method will return them without the last position (for example position 2 will not return a list with position 44)
     */
    private List<Integer> getPotentialPositions(int position) {
        List<Integer> positionList = new ArrayList<>();
        if (position == 2 || position == 4 || position == 6) {
            for (int i = position; i <= position + 35; i += 7) {
                positionList.add(i);
            }
        } else if (position == 44 || position == 46 || position == 48) {
            for (int i = position; i >= position - 35; i -= 7) {
                positionList.add(i);
            }
        } else if (position == 8 || position == 22 || position == 36) {
            for (int i = position; i <= position + 5; i++) {
                positionList.add(i);
            }
        } else if (position == 14 || position == 28 || position == 42) {
            for (int i = position; i >= position - 5; i--) {
                positionList.add(i);
            }
        }
        return positionList;
    }
}
