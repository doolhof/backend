package be.kdg.backend.services.game.interfaces;


import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Tile;
import be.kdg.backend.domain.game.Treasure;

import java.util.List;

/**
 * This is the interface for GameBoardService
 */
public interface GameBoardService {
    GameBoard createGameBoard(List<Treasure> treasures);

    GameBoard findGameBoard(int id);

    GameBoard saveGameBoard(GameBoard gameBoard);

    int getDisabledArrow(int arrowNumber);


}
