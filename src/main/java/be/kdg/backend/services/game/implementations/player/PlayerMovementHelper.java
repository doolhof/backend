package be.kdg.backend.services.game.implementations.player;

import be.kdg.backend.domain.game.GameBoard;
import be.kdg.backend.domain.game.Tile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is responsible for calculating all the possible paths towards a destination tile
 */
@Component
public class PlayerMovementHelper {
    /**
     * This method calculates all the possible paths based on the begin tile and destination tile
     *
     * @param gameBoard       the gameBoard the player's on
     * @param beginTile       the tile the player is currently standing on
     * @param destinationTile the tile the player want to move to
     * @return a list, with all the potential paths to the destination tile
     */
    public LinkedList<LinkedList<Tile>> calculatePaths(Tile beginTile, Tile destinationTile, GameBoard gameBoard) {
        LinkedList<LinkedList<Tile>> paths = new LinkedList<>();
        LinkedList<LinkedList<Tile>> unfinishedPaths = new LinkedList<>();
        unfinishedPaths.add(new LinkedList<>(Arrays.asList(beginTile)));

        while (!unfinishedPaths.isEmpty()) {
            for (int i = 0; i < unfinishedPaths.size(); i++) {
                LinkedList<Tile> currentPath = unfinishedPaths.get(i);
                List<Tile> pathTiles = findOpenWalls(currentPath.getLast(), gameBoard.getTiles()).stream()
                        .filter(list -> currentPath.stream().noneMatch(o -> o.getPosition() == list.getPosition())).
                                collect(Collectors.toList());

                unfinishedPaths.remove(i);
                if (pathTiles.size() >= 1) {
                    for (Tile tile : pathTiles) {
                        LinkedList<Tile> newPath = new LinkedList<>(currentPath);
                        newPath.add(tile);
                        unfinishedPaths.add(i, newPath);

                        if (tile.getPosition() == destinationTile.getPosition()) {
                            paths.add(unfinishedPaths.get(i));
                        }
                    }
                }
            }
        }
        return paths;
    }

    private List<Tile> findOpenWalls(Tile tile, List<Tile> gameboard) {
        List<Tile> tiles = new ArrayList<>();
        //check if a path exists between current tile and the tile EAST of current tile
        if (!tile.getMazeCard().isEastWall() && tile.getPosition() % 7 != 0) {
            Tile newTile = (Tile) gameboard.stream().filter(eastTile -> eastTile.getPosition() == tile.getPosition() + 1).toArray()[0];
            if (!newTile.getMazeCard().isWestWall()) {
                tiles.add(newTile);
            }
        }
        //check if a path exists between current tile and the tile WEST of current tile
        if (!tile.getMazeCard().isWestWall() && tile.getPosition() % 7 != 1) {
            Tile newTile = (Tile) gameboard.stream().filter(westTile -> westTile.getPosition() == tile.getPosition() - 1).toArray()[0];
            if (!newTile.getMazeCard().isEastWall()) {
                tiles.add(newTile);
            }
        }
        //check if a path exists between current tile and the tile NORTH of current tile
        if (!tile.getMazeCard().isNorthWall() && tile.getPosition() - 7 > 0) {
            Tile newTile = (Tile) gameboard.stream().filter(westTile -> westTile.getPosition() == tile.getPosition() - 7).toArray()[0];
            if (!newTile.getMazeCard().isSouthWall()) {
                tiles.add(newTile);
            }
        }
        //check if a path exists between current tile and the tile SOUTH of current tile
        if (!tile.getMazeCard().isSouthWall() && tile.getPosition() + 7 <= 49) {
            Tile newTile = (Tile) gameboard.stream().filter(westTile -> westTile.getPosition() == tile.getPosition() + 7).toArray()[0];
            if (!newTile.getMazeCard().isNorthWall()) {
                tiles.add(newTile);
            }
        }
        return tiles;
    }
}
