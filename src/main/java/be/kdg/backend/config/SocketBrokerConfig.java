package be.kdg.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class SocketBrokerConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/game-party-socket", "/chat-socket", "/game-socket").setAllowedOrigins("*").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/chat", "/lobby", "/game", "/topic");
        registry.setApplicationDestinationPrefixes("/playerChat", "/party", "/gameParty", "/app",
                "remaining", "moveMazeCard", "movePlayerTile", "movePawn", "getPlayer", "endTurn",
                "disabledArrow", "checkWinGame");
    }
}