package be.kdg.backend.exceptions;

public class MazeCardServiceException extends RuntimeException {
    public MazeCardServiceException(String message) {
        super(message);
    }
}
