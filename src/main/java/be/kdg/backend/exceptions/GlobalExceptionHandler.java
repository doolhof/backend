package be.kdg.backend.exceptions;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {InvalidMoveException.class})
    protected ResponseEntity<?> handleInvalidMove(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, "No path found for destination.",
                new HttpHeaders(),
                HttpStatus.NO_CONTENT, request);
    }

    @ExceptionHandler(value = {TooManyPlayersException.class})
    protected ResponseEntity<?> handleTooManyPlayers(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, "Too many players to create a game.",
                new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = {CreatePlayerException.class})
    protected ResponseEntity<?> handleCreatePlayer(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {
            GameNotFoundException.class,
            LobbyNotFoundException.class
    })
    protected ResponseEntity<?> handleNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {
            UserNotFoundException.class,
            FriendNotFoundException.class
    })
    protected ResponseEntity<?> handleSearchNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {AlreadyFriendsException.class})
    protected ResponseEntity<?> handleAlreadyFriends(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}