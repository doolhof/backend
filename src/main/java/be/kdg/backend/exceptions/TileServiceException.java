package be.kdg.backend.exceptions;

public class TileServiceException extends RuntimeException {
    public TileServiceException(String message) {
        super(message);
    }
}
