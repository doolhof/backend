package be.kdg.backend.exceptions;

public class LobbyServiceException extends RuntimeException{
    public LobbyServiceException(String message) {
        super(message);
    }
}
