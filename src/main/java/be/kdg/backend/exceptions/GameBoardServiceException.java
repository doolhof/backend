package be.kdg.backend.exceptions;

public class GameBoardServiceException extends RuntimeException {
    public GameBoardServiceException(String message)
    {
        super(message);
    }
}
