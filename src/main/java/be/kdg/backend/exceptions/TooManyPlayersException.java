package be.kdg.backend.exceptions;

public class TooManyPlayersException extends RuntimeException {
    public TooManyPlayersException(String message)
    {
        super(message);
    }

}
