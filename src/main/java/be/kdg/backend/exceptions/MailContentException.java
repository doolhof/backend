package be.kdg.backend.exceptions;

public class MailContentException extends RuntimeException {
    public MailContentException(String message) {
        super(message);
    }
}
