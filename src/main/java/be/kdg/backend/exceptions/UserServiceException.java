package be.kdg.backend.exceptions;

public class UserServiceException extends RuntimeException
{

    /**
     * When something goes wrong with the log-in this error gets thrown
     * @param message
     */
    public UserServiceException(String message)
    {
        super(message);
    }
}
