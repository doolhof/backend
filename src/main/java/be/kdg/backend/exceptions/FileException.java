package be.kdg.backend.exceptions;

public class FileException extends RuntimeException  {
    public FileException(String message) {
        super(message);
    }
}
