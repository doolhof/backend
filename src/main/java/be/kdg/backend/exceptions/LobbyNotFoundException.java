package be.kdg.backend.exceptions;

public class LobbyNotFoundException extends RuntimeException {
    public LobbyNotFoundException(String msg) {
        super(msg);
    }
}
