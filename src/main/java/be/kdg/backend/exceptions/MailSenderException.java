package be.kdg.backend.exceptions;

public class MailSenderException extends RuntimeException {
    public MailSenderException(String message) {
        super(message);
    }
}
