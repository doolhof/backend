package be.kdg.backend.exceptions;

public class CreatePlayerException extends RuntimeException {
    public CreatePlayerException(String message) {
        super(message);
    }
}
