package be.kdg.backend;

import be.kdg.backend.domain.user.Role;
import be.kdg.backend.domain.user.User;
import be.kdg.backend.domain.user.UserSetting;
import be.kdg.backend.domain.user.UserStatistic;
import be.kdg.backend.exceptions.FileException;
import be.kdg.backend.services.user.interfaces.UserService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class BackendApplication implements CommandLineRunner {

    @Autowired
    UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
        try {
            FileInputStream serviceAccount = new FileInputStream("src\\main\\resources\\firebase\\mazetrix.json");

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://mazetrix-c09ac.firebaseio.com")
                    .build();

            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }

        } catch (IOException e) {
            throw new FileException("Could not read file path.");
        }

    }


    @Override
    public void run(String... params) throws Exception {
        User admin = new User();
        admin.setUsername("admin");
        admin.setEncryptedPassword("admin");
        admin.setEmail("leonora@live.be");
        admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_ADMIN)));
        admin.setUserSetting(new UserSetting());
        userService.saveUser(admin);

        User admin2 = new User();
        admin2.setUsername("root");
        admin2.setEncryptedPassword("root");
        admin2.setEmail("root@root.com");
        admin2.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_ADMIN)));
        admin2.setUserSetting(new UserSetting(true,true,true,true,false));
        userService.saveUser(admin2);

        User testUser = new User();
        testUser.setUsername("Jan");
        testUser.setEncryptedPassword("jan");
        testUser.setEmail("jan@jan.com");
        testUser.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT)));
        testUser.setUserStatistic(new UserStatistic(70,50,20));
        testUser.setUserSetting(new UserSetting(true,true,true,true,false));
        userService.saveUser(testUser);

        User testUser2 = new User();
        testUser2.setUsername("Jos");
        testUser2.setEncryptedPassword("jos");
        testUser2.setEmail("jos@jos.com");
        testUser2.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT)));
        testUser2.setUserSetting(new UserSetting());
        userService.saveUser(testUser2);

    }

}

